﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_TraCuu
    {
        DAO_TraCuu hs = new DAO_TraCuu();
        public List<FootBall_Club> load_DoiBong()
        {
            return hs.load_DoiBong();
        }
        public List<LoaiCauThu> load_LoaiCT()
        {
            return hs.load_LoaiCT();
        }
        public DataTable TimKiem_DS(string ID_DB, string loaiCT)
        {
            return hs.TimKiem_DS(ID_DB, loaiCT);
        }
        public DataTable TimKiem(string TenCT)
        {
            return hs.TimKiem(TenCT);
        }
    }
}
