﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_KetQua
    {
        DAO_KetQua hs = new DAO_KetQua();
        public List<TranDau> load_TranDau(int VongDau)
        {
            return hs.load_TranDau(VongDau);
        }
        public List<int> lst_VongDau()
        {
            return hs.lst_VongDau();
        }
        public DataTable lst_BanThang(string MaTranDau)
        {
            return hs.lst_BanThang(MaTranDau);
        }
        public void UPDATE(string MaTranDau, string goal1, string goal2, string TenCT, string LoaiBT, int ThoiDiem)
        {
            hs.UPDATE(MaTranDau, goal1, goal2, TenCT, LoaiBT, ThoiDiem);
        }
        public bool KT_ThoiDiem(int ThoiDiem)
        {
            return hs.KT_ThoiDiem(ThoiDiem);
        }
        public bool KT_LoaiBanThang(string LoaiBT)
        {
            return hs.KT_LoaiBanThang(LoaiBT);
        }
    }
}
