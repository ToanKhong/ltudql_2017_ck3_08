﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class BUS_TiepNhanHoSoDangKi
    {
        DAO_TiepNhanHoSoDangKi Hoso = new DAO_TiepNhanHoSoDangKi();
        public DataTable Load_Player(string DoiBong)
        {
            return Hoso.load_Player(DoiBong);
        }     
        public List<FootBall_Club> lstDoiBong()
        {
            return Hoso.lstDoiBong();
        }
        public void Insert_Player(Player pl,string DoiBong)
        {
            Hoso.Insert_Player(pl,DoiBong);
        }
        public void Insert_DoiBong(string DB, string SVD)
        {
            Hoso.Insert_Football(DB, SVD);
        }

        public void Delete_DoiBong(string ID_DB)
        {
            Hoso.Delete_DoiBong(ID_DB);
        }
        public void Delete_Player(string ID_Player)
        {
            Hoso.Delete_Player(ID_Player);
        }
        public void UPDATE(string ID_Player,Player pl,FootBall_Club fc)
        {
            Hoso.UpDate(ID_Player, pl, fc);
        }
        public bool KT_DoiBong(string doibong)
        {
            return Hoso.KT_DoiBong(doibong);
        }        
    }
}
