﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class BUS_LichThiDau
    {
        DAO_LichThiDau Hoso = new DAO_LichThiDau();
        public DataTable Lich()
        {
            return Hoso.BangThiDau();
        }
        public bool KT_LichThiDau()
        {
            return Hoso.KT_LichThiDau();
        }
        public void XoaLichThiDau()
        {
            Hoso.XoaLichThiDau();
        }
        public bool KT_DoiBong()
        {
            return Hoso.KT_DoiBong();
        }
    }
}
