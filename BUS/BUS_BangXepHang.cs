﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAO;
namespace BUS
{
    public class BUS_BangXepHang
    {
        DAO_BangXepHang hs = new DAO_BangXepHang();
        public List<string> load_NgayTD()
        {
            return hs.load_NgayTD();
        }
        public DataTable BangXepHang(string Ngay)
        {
            return hs.BangXepHang(Ngay);
        }
    }
}
