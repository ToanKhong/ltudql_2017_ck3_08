﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;

namespace BUS
{
    public class BUS_QuyDinh
    {
        DAO_QuyDinh hs = new DAO_QuyDinh();
        public List<QuyDinh> QuyDinh_Default()
        {
            return hs.QuyDinh_Default();
        }
        public List<QuyDinh> QuyDinh_MoiNhat()
        {
            return hs.QuyDinh_MoiNhat();
        }
        public List<LoaiBanThang> Lst_LoaiBT()
        {
            return hs.Lst_LoaiBT();
        }
        public void UPDate(QuyDinh qd)
        {
            hs.UPDate(qd);
        }
        public void Insert_LoaiBT(string ten)
        {
            hs.Insert_LoaiBT(ten);
        }
        public void Delete_LoaiBT(string id)
        {
            hs.Delete_LoaiBT(id);
        }
    }
}
