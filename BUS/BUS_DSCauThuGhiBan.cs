﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using System.Data;
namespace BUS
{
    public class BUS_DSCauThuGhiBan
    {
        DAO_DSCauThuGhiBan hs = new DAO_DSCauThuGhiBan();
        public List<string> load_NgayTD()
        {
            return hs.load_NgayTD();
        }
        public DataTable DSCauThu_GhiBan(string NgayBD,string NgayKT)
        {
            return hs.DSCauThu_GhiBan(NgayBD,NgayKT);
        }
    }
}
