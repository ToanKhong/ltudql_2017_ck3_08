﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
namespace DAO
{
    public class DAO_TiepNhanHoSoDangKi
    {
        List<int> a = new List<int>();
        DataTable dt = new DataTable();
        public DataTable load_Player(string DoiBong)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var lst_Player = (from PL in db.PLAYERs
                                  join ML in db.LOAICAUTHUs on PL.MaLoaiCT equals ML.MaLoaiCT
                                  join DB in db.DOIBONGs on PL.ID_DB equals DB.ID_DB
                                  where DB.TenDB.Trim() == DoiBong
                                  select new
                                  {
                                      PL.ID_Player,
                                      PL.Ten_Player,
                                      PL.NgaySinh,
                                      PL.MaLoaiCT,
                                      ML.TenLoaiCT,
                                      PL.GhiChu,
                                      PL.ID_DB
                                  });
                dt.Columns.Clear();
                dt.Rows.Clear();
                dt.Columns.Add(new DataColumn("STT", typeof(int)));
                dt.Columns.Add(new DataColumn("Mã Cầu Thủ", typeof(string)));
                dt.Columns.Add(new DataColumn("Tên Cầu Thủ", typeof(string)));
                dt.Columns.Add(new DataColumn("Ngày Sinh", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("Loại Cầu Thủ", typeof(string)));
                dt.Columns.Add(new DataColumn("Ghi Chú", typeof(string)));
                int i = 1;
                foreach (var item in lst_Player)
                {
                    DataRow row = dt.NewRow();
                    row[0] = i;
                    row[1] = item.ID_Player;
                    row[2] = item.Ten_Player;
                    row[3] = item.NgaySinh.Value.Date;
                    row[4] = item.TenLoaiCT;
                    row[5] = item.GhiChu;
                    i += 1;
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        public List<FootBall_Club> lstDoiBong()
        {
            List<FootBall_Club> lstDB = new List<FootBall_Club>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = from DB in db.DOIBONGs
                           select DB;
                foreach (DOIBONG DB in item)
                {
                    var SoLuong = from pl in db.PLAYERs
                                  where pl.ID_DB == DB.ID_DB
                                  select pl; 
                    lstDB.Add(new FootBall_Club(DB.ID_DB, DB.TenDB, DB.SanVD,SoLuong.Count()));
                }
            }
            return lstDB;
        }
        void Tao_ID_DB()
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                a.Clear();
                var lst = from DB in db.DOIBONGs
                          select DB;
                foreach (DOIBONG x in lst)
                {
                    a.Add(Int32.Parse(x.ID_DB.Substring(3)));
                }
            }

        }
        public void Insert_Football(string DB, string SVD)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                Tao_ID_DB();
                if (KT_DoiBong(DB))
                {
                    DOIBONG fc = new DOIBONG();
                    int i = 1;
                    while (i > 0)
                    {
                        if (!a.Contains(i))
                        {
                            if (i < 9)
                            {
                                fc.ID_DB = "DB00" + i.ToString();
                                fc.TenDB = DB;
                                fc.SanVD = SVD;

                            }
                            else
                            {
                                fc.ID_DB = "DB0" + i.ToString();
                                fc.TenDB = DB;
                                fc.SanVD = SVD;
                            }
                            db.DOIBONGs.InsertOnSubmit(fc);
                            db.SubmitChanges();
                            break;                            
                        }
                        i++;
                    }               
                }
            }
        }

        void Tao_ID_Player()
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                a.Clear();
                var lst = from PL in db.PLAYERs                         
                          select PL;
                foreach (PLAYER x in lst)
                {
                    a.Add(Int32.Parse(x.ID_Player.Substring(3)));
                }
            }
           
        }
        public void Insert_Player(Player pl, string TenDB)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {                              
                Tao_ID_Player();               
                var item = from DB in db.DOIBONGs
                           where DB.TenDB.Trim() ==TenDB
                           select DB;
                PLAYER CT = new PLAYER();
                foreach (DOIBONG b in item)
                {
                    CT.ID_DB = b.ID_DB;
                }
                int i = 1;
                while (i > 0)
                {
                    if (!a.Contains(i))
                    {
                        if (i < 9)
                        {
                            CT.ID_Player = "CT00" + i.ToString();
                            CT.Ten_Player = pl.Ten_Player;
                            CT.NgaySinh = pl.NgaySinh;
                            CT.MaLoaiCT = "L0" + pl.LoaiCT;
                            CT.GhiChu = pl.GhiChu;                          
                        }
                        else
                        {
                            CT.ID_Player = "CT0" + i.ToString();
                            CT.Ten_Player = pl.Ten_Player;
                            CT.NgaySinh = pl.NgaySinh;
                            CT.MaLoaiCT = "L0" + pl.LoaiCT;
                            CT.GhiChu = pl.GhiChu;                          
                        }
                        db.PLAYERs.InsertOnSubmit(CT);
                        db.SubmitChanges();
                        break;
                    }
                    i++;
                }              
            }
        }
        public void Delete_DoiBong(string ID_DB)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var lst = from pl in db.PLAYERs
                          where pl.ID_DB == ID_DB
                          select pl;
                foreach(PLAYER i in lst)
                {
                    db.PLAYERs.DeleteOnSubmit(i);
                    db.SubmitChanges();
                }
                DOIBONG item = db.DOIBONGs.Single(DOIBONG => DOIBONG.ID_DB == ID_DB);
                db.DOIBONGs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }         
        }
        public void Delete_Player(string ID_Player)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                PLAYER item = db.PLAYERs.Single(PLAYER => PLAYER.ID_Player == ID_Player);
                db.PLAYERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }
        }
        public void UpDate(string ID_Player,Player pl, FootBall_Club fc)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                DOIBONG DB = db.DOIBONGs.Single(p => p.ID_DB == fc.ID_DB);
                DB.TenDB = fc.TenDB;
                DB.SanVD = fc.SanVĐ;
                PLAYER PL = db.PLAYERs.Single(p => p.ID_Player == ID_Player);
                PL.Ten_Player = pl.Ten_Player;
                PL.NgaySinh = pl.NgaySinh;
                PL.MaLoaiCT = "L0" + pl.LoaiCT;
                PL.GhiChu = pl.GhiChu;

                db.SubmitChanges();
            }
        }
        public bool KT_DoiBong(string DoiBong)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var lst_DoiBong = (from DB in db.DOIBONGs
                                   where DB.TenDB.Trim() == DoiBong.Trim()
                                   select DB);
                if (!lst_DoiBong.Any())
                    return true;
                return false;
            }
        }    
        public bool KT_Player(string ID_Player)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var lst_Player = (from PL in db.PLAYERs
                                  join DB in db.DOIBONGs
                                  on PL.ID_DB equals DB.ID_DB
                                  where PL.ID_Player.Trim() == ID_Player
                                  select PL);
                if (!lst_Player.Any())
                    return true;
                return false;
            }
        }
    }
}
