﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;

namespace DAO
{
    public class DAO_QuyDinh
    {
        QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext();
        public List<QuyDinh> QuyDinh_Default()
        {
            var item = from QD in db.QUYDINHs
                       where QD.ID_ThamSo == 0
                       select QD;        
             List<QuyDinh> lst = new List<QuyDinh>();
            foreach(QUYDINH q in item)
            {
                lst.Add(new QuyDinh(Convert.ToInt16(q.TuoiDoiDa), Convert.ToInt16(q.TuoiToiThieu),
                    Convert.ToInt16(q.SoCauThuToiDa), Convert.ToInt16(q.SoCauThuToiThieu),
                    Convert.ToInt16(q.SoCauThuNuocNgoaiToiDa), Convert.ToInt16(q.ThoiDiemGhiBanToiDa)
                    , Convert.ToInt16(q.DiemSoThang), Convert.ToInt16(q.DiemSoHoa),
                     Convert.ToInt16(q.DiemSoThua)));
            }
            return lst;
        }
        public List<QuyDinh> QuyDinh_MoiNhat()
        {
            var item = (from QD in db.QUYDINHs 
                        orderby QD.ID_ThamSo descending                      
                        select QD);
            List<QuyDinh> lst = new List<QuyDinh>();
            foreach (QUYDINH q in item)
            {
                lst.Add(new QuyDinh(Convert.ToInt16(q.TuoiDoiDa), Convert.ToInt16(q.TuoiToiThieu),
                    Convert.ToInt16(q.SoCauThuToiDa), Convert.ToInt16(q.SoCauThuToiThieu),
                    Convert.ToInt16(q.SoCauThuNuocNgoaiToiDa), Convert.ToInt16(q.ThoiDiemGhiBanToiDa)
                    , Convert.ToInt16(q.DiemSoThang), Convert.ToInt16(q.DiemSoHoa),
                     Convert.ToInt16(q.DiemSoThua)));
            }
            return lst;
        }
        public List<LoaiBanThang> Lst_LoaiBT()
        {
            var item = from BT in db.LOAIBANTHANGs
                       select BT;
            List<LoaiBanThang> lst = new List<LoaiBanThang>();
            foreach(LOAIBANTHANG lt in item)
            {
                lst.Add(new LoaiBanThang(lt.MaLoaiBanThang, lt.TenLoaiBanThang));
            }
            return lst;
        }

        public void UPDate(QuyDinh qd)
        {          
            int i = 1;
            while (i > 0)
            {
                var id = from QD in db.QUYDINHs
                         where QD.ID_ThamSo == i
                         select QD;
                if(!id.Any())
                {
                    QUYDINH item = new QUYDINH();
                    item.ID_ThamSo = i;
                    item.TuoiDoiDa = qd.Tuoi_Max;
                    item.TuoiToiThieu = qd.Tuoi_Min;
                    item.SoCauThuToiDa = qd.SoLuong_Max;
                    item.SoCauThuToiThieu = qd.SoLuong_Min;
                    item.SoCauThuNuocNgoaiToiDa = qd.CTN_MAX;
                    item.ThoiDiemGhiBanToiDa = qd.Time_Max;
                    item.DiemSoThang = qd.Thang;
                    item.DiemSoHoa = qd.Hoa;
                    item.DiemSoThua = qd.Thua;

                    db.QUYDINHs.InsertOnSubmit(item);
                    db.SubmitChanges();
                    break;
                }
                i++;
            }           
        }
        public void Insert_LoaiBT(string ten)
        {
            int i = 1;
            while (i > 0)
            {
                var id = from lt in db.LOAIBANTHANGs
                         where lt.MaLoaiBanThang == i.ToString()
                         select lt;
                if (!id.Any())
                {
                    LOAIBANTHANG l = new LOAIBANTHANG();
                    l.MaLoaiBanThang = i.ToString();
                    l.TenLoaiBanThang = ten;

                    db.LOAIBANTHANGs.InsertOnSubmit(l);
                    db.SubmitChanges();
                    break;
                }
                i++;
            }
        }
        public void Delete_LoaiBT(string id)
        {
            var item = from BT in db.LOAIBANTHANGs
                       where  BT.MaLoaiBanThang == id
                       select BT;
            if (item.Any())
            {
                foreach (LOAIBANTHANG lt in item)
                {
                    db.LOAIBANTHANGs.DeleteOnSubmit(lt);
                    db.SubmitChanges();
                }
            }        
           
        }
    }
}
