﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;

namespace DAO
{
    public class DAO_KetQua
    {
        DataTable dt = new DataTable();
        public List<TranDau> load_TranDau(int VongDau)
        {
            List<TranDau> lst = new List<TranDau>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var Tran_Dau = from LTD in db.TRANDAUs
                               join DB1 in db.DOIBONGs on LTD.Doi1 equals DB1.ID_DB
                               join DB2 in db.DOIBONGs on LTD.Doi2 equals DB2.ID_DB
                               where LTD.VongTD == VongDau
                               orderby LTD.MaTranDau ascending
                               select new
                               {
                                   DB1,
                                   DB2,
                                   LTD
                               };
                if (Tran_Dau.Any())
                {
                    foreach (var item in Tran_Dau)
                    {
                        if (item.LTD.TySo == null)
                        {
                            lst.Add(new TranDau(item.LTD.MaTranDau, Convert.ToInt16(item.LTD.VongTD), item.DB1.TenDB, item.DB2.TenDB,
                                          null, null,item.LTD.NgayThiDau.Value.ToShortDateString(), item.LTD.GioThiDau.ToString(), item.LTD.SanVD));
                        }
                        else
                        {
                            lst.Add(new TranDau(item.LTD.MaTranDau, Convert.ToInt16(item.LTD.VongTD), item.DB1.TenDB, item.DB2.TenDB,
                                          item.LTD.TySo.Substring(0, 1), item.LTD.TySo.Substring(2, 1),
                                           item.LTD.NgayThiDau.Value.ToShortDateString(), item.LTD.GioThiDau.ToString(), item.LTD.SanVD));
                        }
                       
                    }
                }
            }
            return lst;
        }
        public List<int> lst_VongDau()
        {
            List<int> lst = new List<int>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = (from td in db.TRANDAUs
                           select td.VongTD).Distinct();
                foreach(var vd in item)
                {
                    lst.Add(vd.Value);
                }
            }
            return lst;                
        }
        public DataTable lst_BanThang(string MaTranDau)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = from bt in db.BANTHANGs
                           join pl in db.PLAYERs on bt.MaCauThu equals pl.ID_Player
                           join DB in db.DOIBONGs on pl.ID_DB equals DB.ID_DB
                           join lbt in db.LOAIBANTHANGs on bt.MaLoaiBanThang equals lbt.MaLoaiBanThang
                           where bt.MaTranDau.Trim() == MaTranDau.Trim()
                          
                           select new
                           {
                               pl.Ten_Player,DB.TenDB,lbt.TenLoaiBanThang,bt.ThoiDiem
                           };
                dt.Columns.Clear();
                dt.Rows.Clear();
                dt.Columns.Add(new DataColumn("STT", typeof(int)));
                dt.Columns.Add(new DataColumn("Cầu Thủ", typeof(string)));
                dt.Columns.Add(new DataColumn("Đội", typeof(string)));
                dt.Columns.Add(new DataColumn("Loại Bàn Thắng", typeof(string)));
                dt.Columns.Add(new DataColumn("Thời Điểm", typeof(string)));
                if (item.Any())
                {
                    int i = 1;
                    foreach (var index in item)
                    {
                        DataRow row = dt.NewRow();
                        row[0] = i;
                        row[1] = index.Ten_Player;
                        row[2] = index.TenDB;
                        row[3] = index.TenLoaiBanThang;
                        row[4] = index.ThoiDiem;

                        dt.Rows.Add(row);
                        i++;
                    }
                }                         
            }
            return dt;                      
        }
        public void UPDATE(string MaTranDau,string goal1,string goal2,string TenCT,string LoaiBT,int ThoiDiem)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                TRANDAU Tyso = db.TRANDAUs.Single(p => p.MaTranDau == MaTranDau);
                Tyso.TySo = goal1 + "-" + goal2;
                var time= from t in db.BANTHANGs
                          where t.ThoiDiem == ThoiDiem & t.MaTranDau == MaTranDau
                          select t;
                PLAYER MaCT = db.PLAYERs.Single(p => p.Ten_Player == TenCT);
                LOAIBANTHANG loai = db.LOAIBANTHANGs.Single(p => p.TenLoaiBanThang == LoaiBT);
                if (!time.Any())
                {
                    int i = 1;
                    while (i > 0)
                    {
                        var kt = from BT in db.BANTHANGs
                                 where Convert.ToInt16(BT.MaBanThang.Substring(2)) == i
                                 select BT;
                        if (!kt.Any())
                        {
                            BANTHANG bt = new BANTHANG();
                            bt.MaBanThang = "BT" + i;
                            bt.MaCauThu = MaCT.ID_Player;
                            bt.MaLoaiBanThang = loai.MaLoaiBanThang;
                            bt.ThoiDiem = ThoiDiem;
                            bt.MaTranDau = MaTranDau;

                            db.BANTHANGs.InsertOnSubmit(bt);
                            break;
                        }
                        i++;
                    }
                }
                else
                {
                    BANTHANG bt = db.BANTHANGs.Single(p => p.MaTranDau == MaTranDau & p.ThoiDiem == ThoiDiem);
                    bt.MaCauThu = MaCT.ID_Player;
                    bt.MaLoaiBanThang = loai.MaLoaiBanThang;
                }
                db.SubmitChanges();
            }
        }
        public bool KT_ThoiDiem(int ThoiDiem)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = db.QUYDINHs.OrderByDescending(p => p.ID_ThamSo).FirstOrDefault();
                if (ThoiDiem > item.ThoiDiemGhiBanToiDa)
                    return true;
                          
            }
            return false;
        }
        public bool KT_LoaiBanThang(string LoaiBT)
        {
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = from l in db.LOAIBANTHANGs
                           where l.TenLoaiBanThang == LoaiBT
                           select l;
                if (!item.Any())
                {
                    return true;
                }             
            }
            return false;
        }
    }
}
