﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;

namespace DAO
{
    public class DAO_TraCuu
    {
        DataTable dt = new DataTable();
        public List<FootBall_Club> load_DoiBong()
        {
            List<FootBall_Club> lst = new List<FootBall_Club>();
            using (QLGVĐBĐQGDataContext db=new DAO.QLGVĐBĐQGDataContext())
            {
                var item = from DB in db.DOIBONGs
                           select DB;              
                foreach(DOIBONG fc in item)
                {
                    lst.Add(new FootBall_Club(fc.ID_DB, fc.TenDB, null, 0));
                }
            }
            return lst;
        }
        public List<LoaiCauThu> load_LoaiCT()
        {
            List<LoaiCauThu> lst = new List<LoaiCauThu>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = from l in db.LOAICAUTHUs
                           select l;
                foreach (LOAICAUTHU lct in item)
                {
                    lst.Add(new LoaiCauThu(lct.MaLoaiCT,lct.TenLoaiCT));
                }
            }
            return lst;
        }
        public DataTable TimKiem(string TenCT)
        {
            dt.Columns.Clear();
            dt.Rows.Clear();
            dt.Columns.Add(new DataColumn("STT", typeof(int)));
            dt.Columns.Add(new DataColumn("Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Đội", typeof(string)));
            dt.Columns.Add(new DataColumn("Loại Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Tổng Số Bàn Thắng", typeof(string)));

            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = from DS in db.TimKiemCauThu(TenCT)
                           select DS;
                int i = 1;
                foreach (var ds in item)
                {
                    DataRow row = dt.NewRow();
                    row[0] = i;
                    row[1] = TenCT;
                    row[2] = ds.TenDB;
                    row[3] = ds.LoaiCT;
                    row[4] = ds.TongSoBT;

                    dt.Rows.Add(row);
                    i++;
                }
            }
            return dt;
        }
        public DataTable TimKiem_DS(string ID_DB, string loaiCT)
        {
            dt.Columns.Clear();
            dt.Rows.Clear();
            dt.Columns.Add(new DataColumn("STT", typeof(int)));
            dt.Columns.Add(new DataColumn("Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Đội", typeof(string)));
            dt.Columns.Add(new DataColumn("Loại Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Tổng Số Bàn Thắng", typeof(string)));

            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext()) {             
                var item = from DS in db.DSCauThu_BanThang(ID_DB, loaiCT)
                           orderby DS.TongSoBT descending
                           select DS;
                int i = 1;
                foreach(var ds in item)
                {
                    DataRow row = dt.NewRow();
                    row[0] = i;
                    row[1] = ds.TenCT;
                    row[2] = ds.TenDB;
                    row[3] = ds.LoaiCT;
                    row[4] = ds.TongSoBT;

                    dt.Rows.Add(row);
                    i++;
                }
            }
            return dt;
        }
    }
}
