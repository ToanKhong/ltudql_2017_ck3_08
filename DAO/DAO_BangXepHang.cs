﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
namespace DAO
{
    public class DAO_BangXepHang
    {
        DataTable dt = new DataTable();
        List<string> lst_DoiBong = new List<string>();
        List<BangXepHang> lst_DS = new List<BangXepHang>();
        public List<string> load_NgayTD()
        {
            List<string> lst = new List<string>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = db.TRANDAUs.GroupBy(p => p.NgayThiDau).Select(x => x.First());
                foreach (var i in item)
                {
                    lst.Add(i.NgayThiDau.Value.ToShortDateString());
                }
            }
            return lst;
        }
        void load_lstDoiBong()
        {
            lst_DoiBong.Clear();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var lst = from DB in db.DOIBONGs
                          select DB.ID_DB;
                foreach (var item in lst)
                {
                    lst_DoiBong.Add(item);
                }
            }
        }
        void load_BangXepHang(string Ngay)
        {
            load_lstDoiBong();
            lst_DS.Clear();      
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                int dthang, dhoa, dthua;
                var qd = db.QUYDINHs.OrderByDescending(p => p.ID_ThamSo).FirstOrDefault();              
                    dthang = Convert.ToInt16(qd.DiemSoThang);
                    dhoa = Convert.ToInt16(qd.DiemSoHoa);
                    dthua = Convert.ToInt16(qd.DiemSoThua);             

                for (int i = 0; i < lst_DoiBong.Count; i++)
                {
                    var item = from DS in db.XepHang(Convert.ToDateTime(Ngay), lst_DoiBong[i])
                               select DS;
                    foreach (var index in item)
                    {
                        int diem = Convert.ToInt16((index.thang * dthang) + (index.hoa * dhoa) + (index.thua * dthua));
                        int hieuso = Convert.ToInt16(index.BanThang - index.BanThua);
                        lst_DS.Add(new BangXepHang(index.ID_DB, Convert.ToInt16(index.thang), Convert.ToInt16(index.hoa), 
                                                    Convert.ToInt16(index.thua),hieuso, diem, 
                                                    Convert.ToInt16(index.BanThang), Convert.ToInt16(index.BanThua), 0));
                    }
                }
            }            
        }      
        void BangXepHang_Rank(string Ngay)
        {
            load_BangXepHang(Ngay);
            SortDesTheoDiem a = new SortDesTheoDiem();           
            lst_DS.Sort(a);
            for(int i = 0; i < lst_DS.Count; i++)
            {
                for(int j = i; j < lst_DS.Count; j++)
                {
                    if (lst_DS[i].Diem == lst_DS[j].Diem &&lst_DS[i].HieuSo<lst_DS[j].HieuSo)
                    {
                        BangXepHang temp = lst_DS[i];
                        lst_DS[i] = lst_DS[j];
                        lst_DS[j] = temp;
                    }

                }
            }
            for (int i = 0; i < lst_DS.Count; i++)
            {
                for (int j = i; j < lst_DS.Count; j++)
                {
                    if (lst_DS[i].Diem == lst_DS[j].Diem && lst_DS[i].HieuSo == lst_DS[j].HieuSo
                        && lst_DS[i].BanThang < lst_DS[j].BanThang)
                    {
                        BangXepHang temp = lst_DS[i];
                        lst_DS[i] = lst_DS[j];
                        lst_DS[j] = temp;
                    }

                }
            }
        }
        public DataTable BangXepHang(string Ngay)
        {
            BangXepHang_Rank(Ngay);
            dt.Columns.Clear();
            dt.Rows.Clear();
            dt.Columns.Add(new DataColumn("STT", typeof(int)));
            dt.Columns.Add(new DataColumn("Đội", typeof(string)));
            dt.Columns.Add(new DataColumn("Thắng", typeof(int)));
            dt.Columns.Add(new DataColumn("Hòa", typeof(int)));
            dt.Columns.Add(new DataColumn("Thua", typeof(int)));
            dt.Columns.Add(new DataColumn("Điểm", typeof(int)));
            dt.Columns.Add(new DataColumn("Hiệu số", typeof(int)));
            dt.Columns.Add(new DataColumn("Bàn Thắng", typeof(int)));
            dt.Columns.Add(new DataColumn("Bàn Thua", typeof(int)));
            dt.Columns.Add(new DataColumn("Hạng", typeof(int)));
            int i = 1;
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                foreach (BangXepHang item in lst_DS)
                {
                    string tenDB = db.DOIBONGs.Single(p => p.ID_DB == item.TenDB).TenDB;
                    DataRow row = dt.NewRow();
                    row[0] = i;
                    row[1] = tenDB;
                    row[2] = item.Thang;
                    row[3] = item.Hoa;
                    row[4] = item.Thua;
                    row[5] = item.Diem;
                    row[6] = item.HieuSo;
                    row[7] = item.BanThang;
                    row[8] = item.BanThua;
                    row[9] = i;
                  
                    dt.Rows.Add(row);
                    i++;
                }
            }          
            return dt;
        }
    }
}
