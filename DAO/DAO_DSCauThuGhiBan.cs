﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAO
{
    public class DAO_DSCauThuGhiBan
    {
        DataTable dt = new DataTable();
        public List<string> load_NgayTD()
        {           
            List<string> lst = new List<string>();
            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {
                var item = db.TRANDAUs.GroupBy(p => p.NgayThiDau).Select(x => x.First());
                foreach(var i in item)
                {
                    lst.Add(i.NgayThiDau.Value.ToShortDateString());
                }
            }
            return lst;
        }
        public DataTable DSCauThu_GhiBan(string NgayBD,string NgayKT)
        {
            dt.Columns.Clear();
            dt.Rows.Clear();
            dt.Columns.Add(new DataColumn("STT", typeof(int)));
            dt.Columns.Add(new DataColumn("Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Đội", typeof(string)));
            dt.Columns.Add(new DataColumn("Loại Cầu Thủ", typeof(string)));
            dt.Columns.Add(new DataColumn("Tổng Số Bàn Thắng", typeof(string)));

            using (QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext())
            {            
                var item = from DS in db.DSCauThu_GhiBan(Convert.ToDateTime(NgayBD),Convert.ToDateTime(NgayKT))
                           orderby DS.TongSoBT descending
                           select DS;
                int i = 1;
                foreach (var ds in item)
                {
                    DataRow row = dt.NewRow();
                    row[0] = i;
                    row[1] = ds.TenCT;
                    row[2] = ds.TenDB;
                    row[3] = ds.LoaiCT;
                    row[4] = ds.TongSoBT;

                    dt.Rows.Add(row);
                    i++;
                }
            }
            return dt;
        }
    }
}
