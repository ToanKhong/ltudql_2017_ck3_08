﻿use master
go
if DB_ID('QLGVD_BongDa_QuocGia') is not null
	drop database QLGVD_BongDa_QuocGia	
go
create database QLGVD_BongDa_QuocGia
go
use QLGVD_BongDa_QuocGia
go

CREATE TABLE DOIBONG
(
	ID_DB nchar(10), 
	TenDB nvarchar(40),
	SanVD nvarchar(30)

	constraint pk_DB primary key(ID_DB)
)

CREATE TABLE LOAICAUTHU
(
	MaLoaiCT nchar(3),
	TenLoaiCT nvarchar(20)

	constraint pk_LoaiCT primary key(MaLoaiCT)
)
CREATE TABLE PLAYER
(
	ID_Player nchar(10),
	Ten_Player nvarchar(50),
	NgaySinh date,
	ID_DB nchar(10),
	MaLoaiCT nchar(3),
	GhiChu nvarchar(30)

	constraint pk_Player primary key(ID_Player)
)

CREATE TABLE TRANDAU
(
	MaTranDau nchar(10),
	Doi1 nchar(10),
	Doi2 nchar(10),
	NgayThiDau date,
	GioThiDau time,
	TySo nchar(10),
	SanVD nvarchar(30),
	VongTD int
	
	constraint pk_LTD primary key(MaTranDau)
)

CREATE TABLE LOAIBANTHANG
(
	MaLoaiBanThang nchar(10),
	TenLoaiBanThang nvarchar(45)

	constraint pk_LBT primary key(MaLoaiBanThang)
)
CREATE TABLE BANTHANG
(
	MaBanThang nchar(10),
	MaCauThu nchar(10),
	MaLoaiBanThang nchar(10),
	ThoiDiem int,
	MaTranDau nchar(10),

	constraint pk_BT primary key(MaBanThang)
)

CREATE TABLE QUYDINH
(
	ID_ThamSo int,
	TuoiToiThieu int,
	TuoiDoiDa int,
	SoCauThuToiThieu int,
	SoCauThuToiDa int,
	SoCauThuNuocNgoaiToiDa int,
	ThoiDiemGhiBanToiDa int,
	DiemSoThang int,
	DiemSoHoa int,
	DiemSoThua int,
	ThuTuUuTienXepHang int

	constraint pk_QD primary key(ID_ThamSo)
)

ALTER TABLE PLAYER ADD CONSTRAINT fk_PLAYER_LOAICT FOREIGN KEY (MaLoaiCT) REFERENCES LOAICAUTHU(MaLoaiCT)
ALTER TABLE PLAYER ADD CONSTRAINT fk_PLAYER_DOIBONG FOREIGN KEY (ID_DB) REFERENCES DOIBONG(ID_DB)
ALTER TABLE TRANDAU ADD CONSTRAINT fk_TRANDAU_DOIBONG_d1 FOREIGN KEY (doi1) REFERENCES DOIBONG(ID_DB)
ALTER TABLE TRANDAU ADD CONSTRAINT fk_TRANDAU_DOIBONG_d2 FOREIGN KEY (doi2) REFERENCES DOIBONG(ID_DB)
ALTER TABLE BANTHANG ADD CONSTRAINT fk_BANTHANG_TRANDAU FOREIGN KEY (MaTranDau) REFERENCES TRANDAU(MaTranDau)
ALTER TABLE BANTHANG ADD CONSTRAINT fk_BANTHANG_PLAYER FOREIGN KEY (MaCauThu) REFERENCES PLAYER(ID_Player)
ALTER TABLE BANTHANG ADD CONSTRAINT fk_BANTHANG_LOAIBT FOREIGN KEY (MaLoaiBanThang) REFERENCES LOAIBANTHANG(MaLoaiBanThang)

INSERT LOAICAUTHU(MaLoaiCT,TenLoaiCT)
VALUES ('L00',N'Cầu thủ ngoại'),
	   ('L01',N'Cầu thủ nội')

INSERT LOAIBANTHANG(MaLoaiBanThang,TenLoaiBanThang)
VALUES ('1',N'Trực tiếp'),
		('2',N'Penalty'),
		('3',N'Phản lưới nhà')
INSERT QUYDINH(ID_ThamSo,TuoiToiThieu,TuoiDoiDa,SoCauThuToiThieu,SoCauThuToiDa,SoCauThuNuocNgoaiToiDa,ThoiDiemGhiBanToiDa,DiemSoThang,DiemSoHoa,DiemSoThua,ThuTuUuTienXepHang)
VALUES (0,16,40,15,22,3,96,3,1,0,1)
GO
INSERT DOIBONG(ID_DB,TenDB,SanVD)
VALUES ('DB001',N'Hà Nội FC',N'Hàng Đẫy'),
	   ('DB002',N'Hồ Chí Minh FC',N'Thống Nhất'),
	   ('DB003',N'Hoàng Anh Gia Lai FC',N'PleiKu'),
	   ('DB004',N'Becamex Bình Dương FC',N'Gò Đậu')
	   --('DB005',N'Sài Gòn FC',N'Thống Nhất B'),
	   --('DB006',N'Viettel FC',N'Hàng Đẫy'),
	   --('DB007',N'Sông Lam Nghệ An FC',N'SVD Vinh'),
	   --('DB008',N'Than Quảng Ninh FC',N'Cẩm Phả'),
	   --('DB009',N'Quảng Nam FC',N'Tam Kỳ'),
	   --('DB010',N'SHB Đà Nẵng FC',N'Hòa Xuân'),
	   --('DB011',N'Dược Nam Hà Nam Định FC',N'Thiên Trường'),
	   --('DB012',N'Hải Phòng FC',N'Lạch Tray'),
	   --('DB013',N'Thanh Hóa FC',N'Thanh Hóa'),
	   --('DB014',N'Sanna Khánh Hòa BVN FC',N'19 tháng 8(Nha Trang)')

INSERT PLAYER(ID_Player,Ten_Player,NgaySinh,ID_DB,MaLoaiCT,GhiChu)
VALUES ('CT001',N'Nguyễn Văn Công','1992-8-1','DB001','L01',null),
	   ('CT002',N'Papa Ibou Kébé','1989-12-10','DB001','L00',null),
	   ('CT003',N'Đoàn Văn Hậu','1999-4-19','DB001','L01',null),
	   ('CT004',N'Moses Oloya','1992-10-22','DB001','L00',null),
	   ('CT005',N'Nguyễn Văn Quyết','1991-6-27','DB001','L01',null),
	   ('CT006',N'Ngyễn Thành Chung','1988-9-10','DB001','L01',null),
	   ('CT007',N'Phạm Thành Lương','1986-9-5','DB001','L01',null),
	   ('CT008',N'Trần Văn Kiên','1996-5-13','DB001','L01',null),
	   ('CT009',N'Phạm Đức Huy','1995-1-20','DB001','L01',null),
	   ('CT010',N'Đinh Tiến Thành','1991-1-24','DB001','L01',null),
	   ('CT011',N'Nguyễn Quang Hải','1997-4-12','DB001','L01',null),
	   ('CT012',N'Pape Omar Faye','1987-1-1','DB001','L00',null),
	   ('CT013',N'NTrần Đình Trọng','1997-4-25','DB001','L01',null),
	   ('CT014',N'Đỗ Duy Mạnh','1996-9-29','DB001','L01',null),
	   ('CT015',N'Đỗ Hùng Dũng ','1993-9-8','DB001','L01',null)

GO
INSERT PLAYER(ID_Player,Ten_Player,NgaySinh,ID_DB,MaLoaiCT,GhiChu)
VALUES ('CT016',N'Trần Phi Sơn','1992-8-1','DB002','L01',null),
	   ('CT017',N'Viktor Prodell','1989-12-10','DB002','L00',null),
	   ('CT018',N'Nguyễn Thanh Thắng','1999-4-19','DB002','L01',null),
	   ('CT019',N'Matías Jadue','1992-10-22','DB002','L00',null),
	   ('CT020',N'Ngô Tùng Quốc','1991-6-27','DB002','L01',null),
	   ('CT021',N'Ngô Hoàng Thịnh','1988-9-10','DB001','L01',null),
	   ('CT022',N'Đỗ Văn Thuận','1986-9-5','DB002','L01',N'Đội Trưởng (C)'),
	   ('CT023',N'Nguyễn Hữu Tuấn','1996-5-13','DB002','L01',null),
	   ('CT024',N'Võ Huy Toàn','1995-1-20','DB002','L01',null),
	   ('CT025',N'Vũ Ngọc Thịnh','1991-1-24','DB002','L01',null),
	   ('CT026',N'Nguyễn Công Phượng','1997-4-12','DB002','L01',null),
	   ('CT027',N'Papé Diakité','1987-1-1','DB002','L00',null),
	   ('CT028',N'Bùi Tiến Dũng','1997-4-25','DB002','L01',null),
	   ('CT029',N'Đặng Quang Huy','1996-9-29','DB002','L01',null),
	   ('CT030',N'Nguyễn Xuân Nam','1993-9-8','DB002','L01',null),
	   ('CT061',N'Vũ Quang Nam','1993-9-8','DB002','L01',null)

GO
INSERT PLAYER(ID_Player,Ten_Player,NgaySinh,ID_DB,MaLoaiCT,GhiChu)
VALUES ('CT031',N'Nguyễn Hữu Anh Tài','1992-8-1','DB003','L01',null),
	   ('CT032',N'Lương Xuân Trường','1989-12-10','DB003','L01',null),
	   ('CT033',N'Nguyễn Phong Hồng Duy','1999-4-19','DB003','L01',null),
	   ('CT034',N'Trần Minh Vương','1992-10-22','DB003','L01',null),
	   ('CT035',N'Nguyễn Văn Toàn','1991-6-27','DB003','L01',null),
	   ('CT036',N'Nguyễn Tuấn Anh','1988-9-10','DB003','L01',N'Đội Trưởng (C)'),
	   ('CT037',N'Trương Trọng Sáng','1986-9-5','DB003','L01',null),
	   ('CT038',N'Vũ Văn Thanh','1996-5-13','DB003','L01',null),
	   ('CT039',N'Hoàng Thanh Tùng','1995-1-20','DB003','L01',null),
	   ('CT040',N'Triệu Việt Hưng','1991-1-24','DB003','L01',null),
	   ('CT041',N'Dụng Quang Nho','1997-4-12','DB003','L01',null),
	   ('CT042',N'Đinh Thanh Bình','1987-1-1','DB003','L01',null),
	   ('CT043',N'Trần Thanh Sơn','1997-4-25','DB003','L01',null),
	   ('CT044',N'Phan Thanh Hậu','1996-9-29','DB003','L01',null),
	   ('CT045',N'A Hoàng','1993-9-8','DB003','L01',null)

GO
INSERT PLAYER(ID_Player,Ten_Player,NgaySinh,ID_DB,MaLoaiCT,GhiChu)
VALUES ('CT046',N'Phạm Văn Tiến','1992-8-1','DB004','L01',null),
	   ('CT047',N'Hedipo Gustavo','1989-12-10','DB004','L00',null),
	   ('CT048',N'Nguyễn Hùng Thiện Đức','1999-4-19','DB004','L01',null),
	   ('CT049',N'Ali Rabo','1992-10-22','DB004','L00',null),
	   ('CT050',N'Nguyễn Thanh Thảo','1991-6-27','DB004','L01',null),
	   ('CT051',N'Hồ Tấn Tài','1988-9-10','DB004','L01',null),
	   ('CT052',N'Nguyễn Trọng Huy','1986-9-5','DB004','L01',null),
	   ('CT053',N'Nguyễn Anh Tài','1996-5-13','DB004','L01',null),
	   ('CT054',N'Ngô Hồng Phước','1995-1-20','DB004','L01',null),
	   ('CT055',N'Nguyễn Tiến Linh','1991-1-24','DB004','L01',null),
	   ('CT056',N'Trần Hữu Đông Triều','1997-4-12','DB004','L01',null),
	   ('CT057',N'Tống Anh Tỷ','1987-1-1','DB004','L01',null),
	   ('CT058',N'Tô Văn Vũ ','1997-4-25','DB004','L01',null),
	   ('CT059',N'Hồ Sỹ Giáp','1996-9-29','DB004','L01',null),
	   ('CT060',N'Nguyễn Anh Đức','1993-9-8','DB004','L01',N'Đội Trưởng (C)')

GO
if OBJECT_ID('DSCauThu_BanThang','FUNCTION') is not null
	drop function DSCauThu_BanThang
GO
Create function DSCauThu_BanThang(@ID_DB nchar(10),@LoaiCT nchar(3))
returns @DSCauThu_BanThang TABLE (TenCT nvarchar(50),TenDB nvarchar(40),LoaiCT nvarchar(20),TongSoBT int)
AS
BEGIN
	declare DSCT cursor
	for(select pl.ID_Player,pl.Ten_Player,db.TenDB,l.TenLoaiCT
		from DOIBONG db,LOAICAUTHU l,PLAYER pl 		
		where pl.ID_DB = @ID_DB and pl.MaLoaiCT = @LoaiCT and db.ID_DB = pl.ID_DB and pl.MaLoaiCT=l.MaLoaiCT)

	open DSCT
	declare @maCT nchar(10),@TenCT nvarchar(50),@TenDB nvarchar(40),@TenLoaiCT nvarchar(20)
	fetch next from DSCT into @maCT,@TenCT,@TenDB,@TenLoaiCT

	while @@FETCH_STATUS = 0
	begin
		declare @count int = 0
		if exists(select * from BANTHANG where MaCauThu = @maCT)
		begin
			select @count = COUNT(*) 
			from BANTHANG bt
			where bt.MaCauThu = @maCT
			group by bt.MaBanThang
		end

		INSERT INTO @DSCauThu_BanThang VALUES(@TenCT,@TenDB,@TenLoaiCT,@count)

		fetch next from DSCT into @maCT,@TenCT,@TenDB,@TenLoaiCT
	end	
	
	close DSCT
	deallocate DSCT

	RETURN
end

GO
if OBJECT_ID('TimKiemCauThu','FUNCTION') is not null
	drop function TimKiemCauThu
go
CREATE FUNCTION TimKiemCauThu(@TenCT nvarchar(50))
returns @DSCauThu_BanThang TABLE (TenCT nvarchar(50),TenDB nvarchar(40),LoaiCT nvarchar(20),TongSoBT int)
AS
BEGIN
	declare DSCT cursor
	for(select pl.ID_Player,db.TenDB,l.TenLoaiCT
		from DOIBONG db,LOAICAUTHU l,PLAYER pl 		
		where db.ID_DB = pl.ID_DB and pl.MaLoaiCT=l.MaLoaiCT and UPPER(pl.Ten_Player) = UPPER(@TenCT))

	open DSCT
	declare @maCT nchar(10),@TenDB nvarchar(40),@TenLoaiCT nvarchar(20)
	fetch next from DSCT into @maCT,@TenDB,@TenLoaiCT

	while @@FETCH_STATUS = 0
	begin
		declare @count int = 0
		if exists(select * from BANTHANG where MaCauThu = @maCT)
		begin
			select @count = COUNT(*) 
			from BANTHANG bt
			where bt.MaCauThu = @maCT
			group by bt.MaBanThang
		end

		INSERT INTO @DSCauThu_BanThang VALUES(@TenCT,@TenDB,@TenLoaiCT,@count)

		fetch next from DSCT into @maCT,@TenDB,@TenLoaiCT
	end	
	
	close DSCT
	deallocate DSCT

	RETURN
END

GO
if OBJECT_ID('DSCauThu_GhiBan','FUNCTION') is not null
	drop function DSCauThu_GhiBan
go
Create function DSCauThu_GhiBan(@NgayBD date,@NgayKT date)
returns @DSCauThu_BanThang TABLE (TenCT nvarchar(50),TenDB nvarchar(40),LoaiCT nvarchar(20),TongSoBT int)
AS
BEGIN
	declare DSCT cursor
	for(select pl.ID_Player,pl.Ten_Player,db.TenDB,l.TenLoaiCT
		from DOIBONG db,LOAICAUTHU l,PLAYER pl	
		where db.ID_DB = pl.ID_DB and l.MaLoaiCT = pl.MaLoaiCT
		and exists (select * from BANTHANG where pl.ID_Player = MaCauThu))

	open DSCT
	declare @maCT nchar(10),@TenCT nvarchar(50),@TenDB nvarchar(40),@TenLoaiCT nvarchar(20)
	fetch next from DSCT into @maCT,@TenCT,@TenDB,@TenLoaiCT

	while @@FETCH_STATUS = 0
	begin
		declare @count int = 0	
			
		select @count = COUNT(*) 
		from BANTHANG bt join TRANDAU td on td.MaTranDau = bt.MaTranDau
		where bt.MaCauThu = @maCT
		group by bt.MaBanThang,td.NgayThiDau
		having td.NgayThiDau <= @NgayKT and td.NgayThiDau >= @NgayBD
		

		INSERT INTO @DSCauThu_BanThang VALUES(@TenCT,@TenDB,@TenLoaiCT,@count)

		fetch next from DSCT into @maCT,@TenCT,@TenDB,@TenLoaiCT
	end	
	
	close DSCT
	deallocate DSCT

	RETURN
end

GO
if OBJECT_ID('XepHang','FUNCTION') is not null
	drop function XepHang
go
Create FUNCTION XepHang(@Ngay date,@ID_DB nchar(10))
returns @DoiBong_TranThang TABLE (ID_DB nchar(10),thang int,hoa int,thua int,BanThang int,BanThua int)
AS
BEGIN
	declare DSTD cursor
	for(select td.MaTranDau,td.TySo
		from TRANDAU td 
		where td.Doi1 = @ID_DB or td.Doi2 = @ID_DB
		group by td.MaTranDau,td.TySo,td.NgayThiDau
		having td.NgayThiDau <= @Ngay)

	open DSTD
	declare @maTD nchar(10),@tyso nchar(10)
	fetch next from DSTD into @maTD,@tyso

	declare @Thang int = 0,@Hoa int = 0,@Thua int = 0,@BanThua int = 0,@BanThang int = 0

	while @@FETCH_STATUS = 0
	begin
		if @tyso is not null
		begin
			declare @goal1 int,@goal2 int
			set @goal1 = convert(int,substring(@tyso,0,2));
			set @goal2 = convert(int,substring(@tyso,3,1))
			if exists(select * from TRANDAU where MaTranDau = @maTD and Doi1 = @ID_DB)
			begin
				if @goal1 > @goal2
					set @Thang+=1
				else
				begin
					if @goal1 < @goal2							
						set @Thua+=1							
					else							
						set @Hoa+=1							
				end
				set @BanThua += @goal2
				set @BanThang += @goal1
			end
			else
			begin
				if @goal1 > @goal2
					set @Thua+=1
				else
				begin
					if @goal1 < @goal2						
						set @Thang+=1						
					else						
						set @Hoa+=1					
				end
				set @BanThua += @goal1
				set @BanThang += @goal2
			end
		end
		fetch next from DSTD into @maTD,@tyso
	end	
	INSERT INTO @DoiBong_TranThang  VALUES(@ID_DB,@Thang,@Hoa,@Thua,@BanThang,@BanThua)

	close DSTD
	deallocate DSTD

	RETURN
end

GO
if OBJECT_ID('Insert_Player','TRiGGER') is not null
	drop TRIGGER Insert_Player
go
CREATE TRIGGER Insert_Player on PLAYER
for insert
AS
BEGIN
	declare @soluongCT int,@soluongCTN int
	set @soluongCT = (select q.SoCauThuToiDa from QUYDINH q
						where q.ID_ThamSo = (select MAX(ID_ThamSo) from QUYDINH))
	set @soluongCTN = (select q.SoCauThuNuocNgoaiToiDa from QUYDINH q
					where q.ID_ThamSo = (select MAX(ID_ThamSo) from QUYDINH))
	if exists(select * from inserted i,PLAYER p where i.ID_Player = p.ID_Player and i.MaLoaiCT = 'L00')
	begin
		if (select COUNT(pl.ID_Player) from PLAYER pl,inserted i 
			where pl.ID_DB = i.ID_DB and pl.MaLoaiCT = 'L00') > @soluongCTN
		begin
			RAISERROR(N'Số lượng cầu thủ nước ngoài không được lớn hơn %d.',16,1,@soluongCTN)
			ROLLBACK
		end	
		else
		begin
			if(select COUNT(pl.ID_Player) from PLAYER pl join inserted i on pl.ID_DB = i.ID_DB) > @soluongCT
			begin
				RAISERROR(N'Số lượng cầu thủ không được lớn hơn %d.',16,1,@soluongCT)
				ROLLBACK
			end
		end
	end
	else
	begin
		if(select COUNT(pl.ID_Player) from PLAYER pl join inserted i on pl.ID_DB = i.ID_DB) > @soluongCT
		begin
			RAISERROR(N'Số lượng cầu thủ không được lớn hơn %d.',16,1,@soluongCT)
			ROLLBACK
		end
	end
END
GO
if OBJECT_ID('Insert_Tuoi','TRiGGER') is not null
	drop TRIGGER Insert_Tuoi
go
CREATE TRIGGER Insert_Tuoi on PLAYER
for insert
AS
BEGIN
	declare @TuoiMax int,@TuoiMin int,@Temp int
	set @TuoiMax = (select q.TuoiDoiDa from QUYDINH q
					where q.ID_ThamSo = (select MAX(ID_ThamSo) from QUYDINH))	
	set @TuoiMin = (select q.TuoiToiThieu from QUYDINH q
					where q.ID_ThamSo = (select MAX(ID_ThamSo) from QUYDINH))	
	set @Temp = (select DATEDIFF(YEAR,i.NgaySinh,GETDATE()) from inserted i)
	if exists(select * from inserted i,PLAYER p where i.ID_Player = p.ID_Player)  				
	begin				
		if (@Temp > @TuoiMax)
		begin
			RAISERROR(N'Tuổi cầu thủ không được lớn hơn %d.',16,1,@TuoiMax)
			ROLLBACK
		end	
		else
		begin
			if (@Temp < @TuoiMin)
			begin
				RAISERROR(N'Tuổi cầu thủ không được nhỏ hơn %d.',16,1,@TuoiMin)
				ROLLBACK
			end
		end			
	end	
END

