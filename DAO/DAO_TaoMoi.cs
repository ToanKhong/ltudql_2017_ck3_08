﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class DAO_TaoMoi
    {
        QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext();
        public int KhoiTao_MuaGiai()
        {
            var bt = from BT in db.BANTHANGs
                     select BT;
            var td = from TD in db.TRANDAUs
                     select TD;
            if (bt.Any())
            {
                db.BANTHANGs.DeleteAllOnSubmit(bt);
                db.TRANDAUs.DeleteAllOnSubmit(td);
                db.SubmitChanges();
                return 1;
            }
            else
            {
                if (td.Any())
                {
                    db.TRANDAUs.DeleteAllOnSubmit(td);
                    db.SubmitChanges();
                    return 1;
                }
            }           
            return -1;
        }

        public int KhoiTao_KetQua_BanThang()
        {
            var bt = from BT in db.BANTHANGs
                     select BT;
            var td = from TD in db.TRANDAUs
                     where TD.TySo != null
                     select TD;
            if (bt.Any())
            {
                db.BANTHANGs.DeleteAllOnSubmit(bt);
                foreach(TRANDAU item in td)
                {
                    item.TySo = null;
                }
                db.SubmitChanges();
                return 1;
            }
            else
            {
                if (td.Any())
                {
                    foreach (TRANDAU item in td)
                    {
                        item.TySo = null;
                        db.SubmitChanges();
                    }
                    return 1;
                }
            }
            return -1;
        }
    }
}
