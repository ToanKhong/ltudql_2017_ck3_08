﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;

namespace DAO
{
    public class DAO_LichThiDau
    {
        QLGVĐBĐQGDataContext db = new QLGVĐBĐQGDataContext();
        DataTable dt = new DataTable();
        List<int> a= new List<int>();
        int[] time = new int[] { 00, 15, 30, 45, 50};
        List<TRANDAU> lich = new List<TRANDAU>();
        List<TRANDAU> luot_di = new List<TRANDAU>();
        List<NgayGio> lst = new List<NgayGio>();
      
        int KT_NamNhuan(int year)
        {
            if (year % 100 == 0)
            {
                if (year % 400 == 0)
                {
                    return 1;
                }             
            }
            else if (year % 4 == 0)
            {
                return 1;
            }
            return 0;
        }
        void Tao_Ngay_Gio()
        {
            var soluong_DB = from DB in db.DOIBONGs
                             select DB;
            int Tuan = 2 * (soluong_DB.Count() - 1);
            int y, m, d, h, s;
            DateTime Data = DateTime.Now;
            y = Data.Year;
            m = 10;
            d = 23;        
            int count = 1;
            Random rd = new Random();
            while (count <= Tuan)
            {
                for(int i = 0; i < 7; i++)
                {                 
                    h = rd.Next(16, 19);
                    s = time[rd.Next(0,4)];
                    if (i == 3)
                    {
                        if ((d == 30 && (m == 4 || m == 9 || m == 11)) || (d == 31 && (m == 1 || m == 3 || m == 10 || m == 12)))
                        {
                            m++;
                            d = 1;
                            if (m == 13)
                            {
                                m = 1;
                                y++;
                            }
                        }       
                        else if (m == 2)
                        {
                            if (KT_NamNhuan(y) != 0)
                            {
                                if(d == 29)
                                {
                                    d = 1;
                                    m++;
                                }
                            }
                            else
                            {
                                if(d == 28)
                                {
                                    d = 1;
                                    m++;
                                }
                            }
                        }
                        else                        
                            d++;                                              
                    }
                    lst.Add(new NgayGio(d, m, y, h, s));
                }
                if (d + 6 > 30 && (m == 4 || m == 9 || m == 11))
                {
                    m++;
                    d = Math.Abs(d + 6 - 30);
                }
                else if (d + 6 > 31 && (m == 1 || m == 3 || m == 10 || m == 12))
                {
                    m++;
                    d = Math.Abs(d + 6 - 31);
                    if (m == 13)
                    {
                        m = 1;
                        y++;
                    }
                }
                else if (m == 2)
                {
                    if (KT_NamNhuan(y) != 0)
                    {
                        if (d + 6 > 29)
                        {
                            m++;
                            d = Math.Abs(d + 6 - 29);
                        }
                    }
                    else
                    {
                        if (d + 6 > 28)
                        {
                            m++;
                            d = Math.Abs(d + 6 - 29);
                        }
                    }
                }
                else
                    d += 6;
                count++;
            }
        }
        void Tao_Mang(int n)
        {
            for(int i = 1; i <= n; i++)
            {
                a.Add(i);
            }
        }
        void Luot_Đi()
        {
            lich.Clear();
            luot_di.Clear();
            var soluong_DB = from DB in db.DOIBONGs
                             select DB;

            int n = soluong_DB.Count();
            int Tuan = n - 1;
            int m = 0;
            int sd = n;
            while (m <= Tuan)
            {
                Tao_Mang(n);
                for (int i = 1; i <= n; i++)
                {                  
                    if (a.Count == 2)
                    {                        
                        TRANDAU l = new TRANDAU();
                        l.MaTranDau = null;
                        l.Doi1 = a[0].ToString();
                        l.Doi2 = a[1].ToString();
                        l.NgayThiDau = null;
                        l.GioThiDau = null;
                        l.TySo = null;
                        l.VongTD = m + 1;
                        l.SanVD = null;
                        lich.Add(l);
                        luot_di.Add(l);
                        a.Clear();
                    }
                    if (a.Contains(i))
                    {
                        for (int j = i; j <= sd; j++)
                        {
                            if (a.Contains(j))
                            {
                                if (i != j)
                                {
                                    int temp = (i + j) % (n - 1);
                                    if (temp == m)
                                    {
                                        if (j > n)
                                        {
                                            if (i + j == m)
                                            {                                                                                              
                                                TRANDAU l = new TRANDAU();
                                                l.MaTranDau = null;
                                                l.Doi1 = i.ToString();
                                                l.Doi2 = j.ToString();
                                                l.NgayThiDau = null;
                                                l.GioThiDau = null;
                                                l.TySo = null;
                                                l.VongTD = m + 1;
                                                l.SanVD = null;
                                                lich.Add(l);
                                                luot_di.Add(l);
                                                a.Remove(i);
                                                a.Remove(j);
                                            }
                                        }
                                        else
                                        {                                        
                                            TRANDAU l = new TRANDAU();
                                            l.MaTranDau = null;
                                            l.Doi1 = i.ToString();
                                            l.Doi2 = j.ToString();
                                            l.NgayThiDau = null;
                                            l.GioThiDau = null;
                                            l.TySo = null;
                                            l.VongTD = m + 1;
                                            l.SanVD = null;
                                            lich.Add(l);
                                            luot_di.Add(l);
                                            a.Remove(i);
                                            a.Remove(j);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                m++;
                sd++;
            }          
        }

        void Tao_Mang()
        {
            a.Clear();
            var soluong_DB = from DB in db.DOIBONGs
                             select DB;
            int Tuan = soluong_DB.Count() - 1;
            int r;
            Random rd = new Random();
            for (int i = 0; i < Tuan; i++)
            {
                do
                {
                    r = rd.Next(1, Tuan + 1);
                } while (a.Contains(r));
                a.Add(r);
            }
        }
        void Luot_Ve()
        {
            Luot_Đi();
            Tao_Mang();
            int m = a.Count;
            for (int i = 0; i < a.Count; i++)
            {
                foreach (TRANDAU item in luot_di)
                {
                    if (a[i] == item.VongTD)
                    {
                        TRANDAU l = new TRANDAU();
                        l.MaTranDau = null;
                        l.Doi1 = item.Doi1;
                        l.Doi2 = item.Doi2;
                        l.NgayThiDau = null;
                        l.GioThiDau = null;
                        l.TySo = null;
                        l.VongTD = m + 1;
                        l.SanVD = null;
                        lich.Add(l);
                        luot_di.Remove(l);
                    }
                }
                m++;
            }
            int count = 1;
            foreach(TRANDAU l in lich)
            {
                if(int.Parse(l.Doi1) > 9 && int.Parse(l.Doi2) > 9)
                {
                    l.Doi1 = "DB0" + l.Doi1;
                    l.Doi2 = "DB0" + l.Doi2;
                }
                else if (int.Parse(l.Doi1) <= 9 && int.Parse(l.Doi2) > 9)
                {
                    l.Doi1 = "DB00" + l.Doi1;
                    l.Doi2 = "DB0" + l.Doi2;
                }
                else if (int.Parse(l.Doi1) > 9 && int.Parse(l.Doi2) <= 9)
                {
                    l.Doi1 = "DB0" + l.Doi1;
                    l.Doi2 = "DB00" + l.Doi2;
                }
                else
                {
                    l.Doi1 = "DB00" + l.Doi1;
                    l.Doi2 = "DB00" + l.Doi2;
                }
                if (count <= 9)
                {
                    l.MaTranDau = "TD00" + count.ToString();
                }
                else if(count >= 10 && count <= 99)
                {
                    l.MaTranDau = "TD0" + count.ToString();
                }
                else
                {
                    l.MaTranDau = "TD" + count.ToString();
                }
                count++;
            }
        }

        void Xep_Lich()
        {
                Luot_Ve();
                Tao_Ngay_Gio();
                int i = 0;
                var SVD = from TRANDAU t in lich
                          join DB in db.DOIBONGs on t.Doi1.Trim() equals DB.ID_DB.Trim()
                          orderby t.MaTranDau ascending
                          select DB.SanVD;
                int k = 0;
                foreach (var item in SVD)
                {
                    lich[k].SanVD = item;
                    k++;
                }
                foreach (TRANDAU item in lich)
                {
                    string dt = lst[i].Xuat_Ngay();
                    item.NgayThiDau = DateTime.ParseExact(dt, "dd/MM/yyyy", null);               
                    item.GioThiDau = Convert.ToDateTime(lst[i].Xuat_Gio()).TimeOfDay;
                    db.TRANDAUs.InsertOnSubmit(item);
                    db.SubmitChanges();
                    i++;
                }         
           
        }
        public void XoaLichThiDau()
        {
            var Data = from LTD in db.TRANDAUs
                       select LTD;
            foreach (TRANDAU item in Data)
            {
                db.TRANDAUs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }
        }
        public bool KT_LichThiDau()
        {
            var item = from LTD in db.TRANDAUs
                       select LTD;
            if (item.Any())
                return true;
            return false;
        }
        public bool KT_DoiBong()
        {
            var lst = from DB in db.DOIBONGs
                      select DB;
            int soluongMax = Convert.ToInt16(db.QUYDINHs.OrderByDescending(p => p.ID_ThamSo).FirstOrDefault().SoCauThuToiDa);
            int soluongMin = Convert.ToInt16(db.QUYDINHs.OrderByDescending(p => p.ID_ThamSo).FirstOrDefault().SoCauThuToiThieu);
            foreach (DOIBONG item in lst)
            {
                int count = db.PLAYERs.Count(p => p.ID_DB == item.ID_DB);
                if (count > soluongMax || count < soluongMin)
                    return true;             
            }
            return false;
        }
        public DataTable BangThiDau()
        {
            Xep_Lich();
            var Tran_Dau = from LTD in db.TRANDAUs
                      join DB1 in db.DOIBONGs on LTD.Doi1 equals DB1.ID_DB
                      join DB2 in db.DOIBONGs on LTD.Doi2 equals DB2.ID_DB
                      orderby LTD.MaTranDau ascending
                      select new
                      {
                          DB1,DB2,LTD
                      };
            dt.Columns.Clear();
            dt.Rows.Clear();
            dt.Columns.Add(new DataColumn("STT", typeof(int)));
            dt.Columns.Add(new DataColumn("Đội 1", typeof(string)));
            dt.Columns.Add(new DataColumn("Đội 2", typeof(string)));
            dt.Columns.Add(new DataColumn("Ngày thi đấu", typeof(string)));
            dt.Columns.Add(new DataColumn("Giờ thi đấu", typeof(string)));
            dt.Columns.Add(new DataColumn("Vòng thi đấu", typeof(int)));
            dt.Columns.Add(new DataColumn("SVĐ", typeof(string)));
            int i = 0;
            foreach (var item in Tran_Dau)
            {                               
                    DataRow row = dt.NewRow();
                    row[0] = i + 1;
                    row[1] = item.DB1.TenDB;
                    row[2] = item.DB2.TenDB;
                    row[3] = item.LTD.NgayThiDau.Value.ToShortDateString();
                    row[4] = item.LTD.GioThiDau;
                    row[5] = item.LTD.VongTD;
                    row[6] = item.LTD.SanVD;
                    i += 1;
                    dt.Rows.Add(row);           
            }
            return dt;
        }
    }
}
