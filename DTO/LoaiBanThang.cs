﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiBanThang
    {
        private string _id;
        private string _TenLoai;

        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string TenLoai
        {
            get
            {
                return _TenLoai;
            }

            set
            {
                _TenLoai = value;
            }
        }
        public LoaiBanThang(string id,string ten)
        {
            this.Id = id;
            this.TenLoai = ten;
        }
    }
}
