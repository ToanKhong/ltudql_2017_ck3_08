﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Player
    {      
        private string _Ten_Player;
        private DateTime _NgaySinh;       
        private int _LoaiCT;
        private string _GhiChu;      

        public string Ten_Player
        {
            get
            {
                return _Ten_Player;
            }

            set
            {
                _Ten_Player = value;
            }
        }      
       
        public int LoaiCT
        {
            get
            {
                return _LoaiCT;
            }

            set
            {
                _LoaiCT = value;
            }
        }

        public string GhiChu
        {
            get
            {
                return _GhiChu;
            }

            set
            {
                _GhiChu = value;
            }
        }

        public DateTime NgaySinh
        {
            get
            {
                return _NgaySinh;
            }

            set
            {
                _NgaySinh = value;
            }
        }

        public Player(string ten_player,DateTime ngaysinh,int loai_player,string ghichu)
        {
            this.Ten_Player = ten_player;
            this.NgaySinh = ngaysinh;           
            this.LoaiCT = loai_player;
            this.GhiChu = ghichu;
        }
    }
}
