﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class FootBall_Club
    {
        private string _ID_DB;
        private string _TenDB;
        private string _SanVĐ;
        private int _SoLuong;

        public string TenDB
        {
            get
            {
                return _TenDB;
            }

            set
            {
                _TenDB = value;
            }
        }

        public string SanVĐ
        {
            get
            {
                return _SanVĐ;
            }

            set
            {
                _SanVĐ = value;
            }
        }

        public string ID_DB
        {
            get
            {
                return _ID_DB;
            }

            set
            {
                _ID_DB = value;
            }
        }

        public int SoLuong
        {
            get
            {
                return _SoLuong;
            }

            set
            {
                _SoLuong = value;
            }
        }

        public FootBall_Club() { }
        public FootBall_Club(string id,string tenĐB,string sanVĐ,int soluong)
        {
            this.ID_DB = id;
            this.TenDB = tenĐB;
            this.SanVĐ = sanVĐ;
            this.SoLuong = soluong;
        }
    }
}
