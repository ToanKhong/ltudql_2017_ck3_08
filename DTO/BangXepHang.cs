﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BangXepHang
    {
        private string _TenDB;
        private int _Thang;
        private int _Hoa;
        private int _Thua;
        private int _HieuSo;
        private int _Diem;
        private int _BanThang;
        private int _BanThua;
        private int _Hang;

        public string TenDB
        {
            get
            {
                return _TenDB;
            }

            set
            {
                _TenDB = value;
            }
        }

        public int Thang
        {
            get
            {
                return _Thang;
            }

            set
            {
                _Thang = value;
            }
        }

        public int Hoa
        {
            get
            {
                return _Hoa;
            }

            set
            {
                _Hoa = value;
            }
        }

        public int Thua
        {
            get
            {
                return _Thua;
            }

            set
            {
                _Thua = value;
            }
        }

        public int HieuSo
        {
            get
            {
                return _HieuSo;
            }

            set
            {
                _HieuSo = value;
            }
        }

        public int Diem
        {
            get
            {
                return _Diem;
            }

            set
            {
                _Diem = value;
            }
        }

        public int BanThang
        {
            get
            {
                return _BanThang;
            }

            set
            {
                _BanThang = value;
            }
        }

        public int BanThua
        {
            get
            {
                return _BanThua;
            }

            set
            {
                _BanThua = value;
            }
        }

        public int Hang
        {
            get
            {
                return _Hang;
            }

            set
            {
                _Hang = value;
            }
        }
        public BangXepHang(string ten,int thang,int hoa,int thua,int hs,int diem,int goal,int bt,int hang)
        {
            this.TenDB = ten;
            this.Thang = thang;
            this.Hoa = hoa;
            this.Thua = thua;
            this.HieuSo = hs;
            this.Diem = diem;
            this.BanThang = goal;
            this.BanThua = bt;
            this.Hang = hang;
        }
    }
    public class SortDesTheoDiem:IComparer<BangXepHang>
    {
        int IComparer<BangXepHang>.Compare(BangXepHang a, BangXepHang b)
        {
            if (a.Diem > b.Diem)
                return -1;
            else if (a.Diem < b.Diem)
                return 1;
            return 0;
        }
    }
}
