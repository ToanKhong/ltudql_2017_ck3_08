﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TranDau
    {
        private string _ID;
        private int _VongTD;
        private string _Doi1;
        private string _Doi2;
        private string _Goal_Doi1;
        private string _Goal_Doi2;
        private string _Ngay;
        private string _Gio;
        private string _SVD;

        public string ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public int VongTD
        {
            get
            {
                return _VongTD;
            }

            set
            {
                _VongTD = value;
            }
        }

        public string Doi1
        {
            get
            {
                return _Doi1;
            }

            set
            {
                _Doi1 = value;
            }
        }

        public string Doi2
        {
            get
            {
                return _Doi2;
            }

            set
            {
                _Doi2 = value;
            }
        }

        public string Goal_Doi1
        {
            get
            {
                return _Goal_Doi1;
            }

            set
            {
                _Goal_Doi1 = value;
            }
        }

        public string Goal_Doi2
        {
            get
            {
                return _Goal_Doi2;
            }

            set
            {
                _Goal_Doi2 = value;
            }
        }

        public string Ngay
        {
            get
            {
                return _Ngay;
            }

            set
            {
                _Ngay = value;
            }
        }

        public string Gio
        {
            get
            {
                return _Gio;
            }

            set
            {
                _Gio = value;
            }
        }

        public string SVD
        {
            get
            {
                return _SVD;
            }

            set
            {
                _SVD = value;
            }
        }
        public TranDau(string id,int vongTD,string doi1,string doi2,string goal1,string goal2,string ngay,string gio,string svd)
        {
            this.ID = id;
            this.VongTD = vongTD;
            this.Doi1 = doi1;
            this.Doi2 = doi2;
            this.Goal_Doi1 = goal1;
            this.Goal_Doi2 = goal2;
            this.Ngay = ngay;
            this.Gio = gio;
            this.SVD = svd;
        }
    }
}
