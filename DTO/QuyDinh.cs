﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class QuyDinh
    {
        private int _Tuoi_Min;
        private int _Tuoi_Max;
        private int _soLuong_Max;
        private int _soLuong_Min;
        private int _CTN_MAX;
        private int _Time_Max;
        private int _Thang;
        private int _Hoa;
        private int _Thua;      

        public int Tuoi_Min
        {
            get
            {
                return _Tuoi_Min;
            }

            set
            {
                _Tuoi_Min = value;
            }
        }

        public int Tuoi_Max
        {
            get
            {
                return _Tuoi_Max;
            }

            set
            {
                _Tuoi_Max = value;
            }
        }

        public int SoLuong_Max
        {
            get
            {
                return _soLuong_Max;
            }

            set
            {
                _soLuong_Max = value;
            }
        }

        public int SoLuong_Min
        {
            get
            {
                return _soLuong_Min;
            }

            set
            {
                _soLuong_Min = value;
            }
        }

        public int CTN_MAX
        {
            get
            {
                return _CTN_MAX;
            }

            set
            {
                _CTN_MAX = value;
            }
        }

        public int Time_Max
        {
            get
            {
                return _Time_Max;
            }

            set
            {
                _Time_Max = value;
            }
        }

        public int Thang
        {
            get
            {
                return _Thang;
            }

            set
            {
                _Thang = value;
            }
        }

        public int Hoa
        {
            get
            {
                return _Hoa;
            }

            set
            {
                _Hoa = value;
            }
        }

        public int Thua
        {
            get
            {
                return _Thua;
            }

            set
            {
                _Thua = value;
            }
        }
    
        public QuyDinh(int TMax,int TMin,int slMax,int slMin,int CTN,int time,int thang,int hoa,int thua)
        {
            this.Tuoi_Max = TMax;
            this.Tuoi_Min = TMin;
            this.SoLuong_Max = slMax;
            this.SoLuong_Min = slMin;
            this.CTN_MAX = CTN;
            this.Time_Max = time;
            this.Thang = thang;
            this.Hoa = hoa;
            this.Thua = thua;           
        }
    }
}
