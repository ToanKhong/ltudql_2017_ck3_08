﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NgayGio
    {
        private int _Ngay;
        private int _Thang;
        private int _Nam;
        private int _Gio;
        private int _phut;

        public int Ngay
        {
            get
            {
                return _Ngay;
            }

            set
            {
                _Ngay = value;
            }
        }

        public int Thang
        {
            get
            {
                return _Thang;
            }

            set
            {
                _Thang = value;
            }
        }

        public int Nam
        {
            get
            {
                return _Nam;
            }

            set
            {
                _Nam = value;
            }
        }

        public int Gio
        {
            get
            {
                return _Gio;
            }

            set
            {
                _Gio = value;
            }
        }

        public int Phut
        {
            get
            {
                return _phut;
            }

            set
            {
                _phut = value;
            }
        }

        public NgayGio(int ngay, int thang, int nam, int gio, int phut)
        {
            this.Ngay = ngay;
            this.Thang = thang;
            this.Nam = nam;
            this.Gio = gio;
            this.Phut = phut;
        }
        public string Xuat_Ngay()
        {
            string date = null;
            if (this.Ngay < 10 && this.Thang < 10)
            {
                date = string.Concat("0" + this.Ngay, "/", "0" + this.Thang, "/", this.Nam);
            }
            else if (this.Ngay >= 10 && this.Thang < 10)
            {
                date = string.Concat(this.Ngay, "/", "0" + this.Thang, "/", this.Nam);
            }
            else if (this.Ngay < 10 && this.Thang >= 10)
            {
                date = string.Concat("0" + this.Ngay, "/", this.Thang, "/", this.Nam);
            }
            else
            {
                date = string.Concat(this.Ngay, "/", this.Thang, "/", this.Nam);
            }
            return date;
        }
        public string Xuat_Gio()
        {
            return string.Concat(this.Gio + " : ", this.Phut);
        }
    }
}
