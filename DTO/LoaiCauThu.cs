﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiCauThu
    {
        private string _id;
        private string _TenLoaiCT;

        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string TenLoaiCT
        {
            get
            {
                return _TenLoaiCT;
            }

            set
            {
                _TenLoaiCT = value;
            }
        }
        public LoaiCauThu(string id,string ten)
        {
            this.Id = id;
            this.TenLoaiCT = ten;
        }
    }
}
