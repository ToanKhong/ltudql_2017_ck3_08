﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class frmThem : Form
    {

        public delegate void SendInformation(string DB, string SVD);
        public SendInformation Sender;
        public frmThem()
        {
            InitializeComponent();
            Sender = new SendInformation(GetInformation);
        }
        private void GetInformation(string DB, string SVD)
        {
            txtDoiBong.Text = DB;
            txtSVĐ.Text = SVD;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtDoiBong.Text == null || txtSVĐ.Text == null || txtTenCT.Text == null ||
                dtpNgaySinh.Text == null ||
                (cboxCTNgoai.CheckState == CheckState.Unchecked && cboxCTNoi.CheckState == CheckState.Unchecked))
            {
                MessageBox.Show("khong hop le");
            }
            else    
            {
                GUI_TiepNhanHoSo frm = new GUI_TiepNhanHoSo();
                if (txtGhiChu.Text == "")
                {
                    if (cboxCTNgoai.Checked)
                    {
                       frm.Sender(txtDoiBong.Text,txtSVĐ.Text,txtTenCT.Text, dtpNgaySinh.Text, 0, null);
                    }
                    else
                    {
                        frm.Sender(txtDoiBong.Text, txtSVĐ.Text,txtTenCT.Text, dtpNgaySinh.Text, 1, null);
                    }
                }
                else
                {
                    if (cboxCTNgoai.Checked)
                    {
                        frm.Sender(txtDoiBong.Text, txtSVĐ.Text,txtTenCT.Text, dtpNgaySinh.Text, 0, null);
                    }
                    else
                    {
                        frm.Sender(txtDoiBong.Text, txtSVĐ.Text,txtTenCT.Text, dtpNgaySinh.Text, 1, null);
                    }
                }               
                this.Close();
            }
          
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
