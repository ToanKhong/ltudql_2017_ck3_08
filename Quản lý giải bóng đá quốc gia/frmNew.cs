﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class frmNew : Form
    {
        BUS_TaoMoi hs = new BUS_TaoMoi();
        public frmNew()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbTaoGiaiDauMoi.CheckState == CheckState.Checked)
            {
                if(hs.KhoiTao_MuaGiai()==1)
                {
                    MessageBox.Show("Khởi tạo mùa giải thành công!!!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                if(hs.KhoiTao_KetQua_BanThang()==1)
                {
                    MessageBox.Show("Khởi tạo thành công!!!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    DialogResult dlr = MessageBox.Show("Mùa giải chưa hiện tại chưa bắt đầu.Bạn có muốn tạo lại lịch thi đấu!!!", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if(dlr == DialogResult.Yes)
                    {
                        GUI_LichThiDau frm = new GUI_LichThiDau();
                        frm.Show();
                        this.Close();
                    }
                }
            }
        }
    }
}
