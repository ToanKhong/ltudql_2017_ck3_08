﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class GUI_DanhSachCauThuGhiBan : Form
    {
        BUS_TraCuu hs = new BUS_TraCuu();
        List<FootBall_Club> lst = null;
        List<LoaiCauThu> lst_loai = null;
        public GUI_DanhSachCauThuGhiBan()
        {
            InitializeComponent();
            lst = hs.load_DoiBong();
            lst_loai = hs.load_LoaiCT();
            cmbDoiBong.DataSource = lst;
            cmbDoiBong.DisplayMember = "TenDB";
            cmbDoiBong.ValueMember = "ID_DB";
            cmbLoai.DataSource = lst_loai;
            cmbLoai.DisplayMember = "TenLoaiCT";
            cmbLoai.ValueMember = "Id";
        }       

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if (txtTenCT.Text != "")
            {
                dgvDanhSach.DataSource = hs.TimKiem(txtTenCT.Text);
            }
            else
                dgvDanhSach.DataSource = hs.TimKiem_DS(cmbDoiBong.SelectedValue.ToString(),cmbLoai.SelectedValue.ToString());
            dgvDanhSach.Columns["STT"].Width = 30;
        }
    }
}
