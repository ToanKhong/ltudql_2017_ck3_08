﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class frmLapBaoCaoGiai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnXacNhan = new System.Windows.Forms.Button();
            this.cbDSCauThuGhiBan = new System.Windows.Forms.CheckBox();
            this.cbBangXepHang = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnXacNhan);
            this.groupBox1.Controls.Add(this.cbDSCauThuGhiBan);
            this.groupBox1.Controls.Add(this.cbBangXepHang);
            this.groupBox1.Location = new System.Drawing.Point(49, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(449, 126);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnXacNhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXacNhan.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnXacNhan.Location = new System.Drawing.Point(326, 47);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(83, 34);
            this.btnXacNhan.TabIndex = 2;
            this.btnXacNhan.Text = "Xác Nhận";
            this.btnXacNhan.UseVisualStyleBackColor = false;
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // cbDSCauThuGhiBan
            // 
            this.cbDSCauThuGhiBan.AutoSize = true;
            this.cbDSCauThuGhiBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDSCauThuGhiBan.ForeColor = System.Drawing.Color.SteelBlue;
            this.cbDSCauThuGhiBan.Location = new System.Drawing.Point(42, 78);
            this.cbDSCauThuGhiBan.Name = "cbDSCauThuGhiBan";
            this.cbDSCauThuGhiBan.Size = new System.Drawing.Size(259, 24);
            this.cbDSCauThuGhiBan.TabIndex = 1;
            this.cbDSCauThuGhiBan.Text = "Danh Sách Cầu Thủ Ghi Bàn";
            this.cbDSCauThuGhiBan.UseVisualStyleBackColor = true;
            // 
            // cbBangXepHang
            // 
            this.cbBangXepHang.AutoSize = true;
            this.cbBangXepHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBangXepHang.ForeColor = System.Drawing.Color.SteelBlue;
            this.cbBangXepHang.Location = new System.Drawing.Point(42, 34);
            this.cbBangXepHang.Name = "cbBangXepHang";
            this.cbBangXepHang.Size = new System.Drawing.Size(155, 24);
            this.cbBangXepHang.TabIndex = 0;
            this.cbBangXepHang.Text = "Bảng Xếp Hạng";
            this.cbBangXepHang.UseVisualStyleBackColor = true;
            // 
            // frmLapBaoCaoGiai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 210);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmLapBaoCaoGiai";
            this.Text = "frmLapBaoCaoGiai";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbDSCauThuGhiBan;
        private System.Windows.Forms.CheckBox cbBangXepHang;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnXacNhan;
    }
}