﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class GUI_Quy_Định : Form
    {
        BUS_QuyDinh QD = new BUS_QuyDinh();
        List<QuyDinh> lst = null;
        List<LoaiBanThang> lst_LBT = null;
        public GUI_Quy_Định()
        {
            InitializeComponent();        
        }
        void load_Default()
        {
            lst = QD.QuyDinh_Default();
            lst_LBT = QD.Lst_LoaiBT();
            nudTuoi_Max.Value = lst[0].Tuoi_Max;
            nudTuoi_Min.Value = lst[0].Tuoi_Min;
            nudCauThu_Max.Value = lst[0].SoLuong_Max;
            nudCauThu_Min.Value = lst[0].SoLuong_Min;
            nudNuocNgoai_Max.Value = lst[0].CTN_MAX;
            nudTime_Max.Value = lst[0].Time_Max;
            nudThang.Value = lst[0].Thang;
            nudHoa.Value = lst[0].Hoa;
            nudThua.Value = lst[0].Thua;
            lstBLoaiBanThang.DataSource = lst_LBT;
            lstBLoaiBanThang.DisplayMember = "TenLoai";
            lstBLoaiBanThang.ValueMember = "Id";
        }

        
        private void btnDefault_Click(object sender, EventArgs e)
        {
            load_Default();
        }

        private void btnUPDATE_Click(object sender, EventArgs e)
        {
            if (nudThang.Value > nudHoa.Value && nudHoa.Value > nudThua.Value)
            {
                QD.UPDate(new QuyDinh(Convert.ToInt16(nudTuoi_Max.Value), Convert.ToInt16(nudTuoi_Min.Value),
                    Convert.ToInt16(nudCauThu_Max.Value), Convert.ToInt16(nudCauThu_Min.Value),
                    Convert.ToInt16(nudNuocNgoai_Max.Value), Convert.ToInt16(nudTime_Max.Value),
                    Convert.ToInt16(nudThang.Value), Convert.ToInt16(nudHoa.Value), Convert.ToInt16(nudThua.Value)));
            }
            else
            {
                MessageBox.Show("Thay đổi điểm số không hợp lệ(thắng > hòa > thua)!!!", "Thông Báo",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        void load_MoiNhat()
        {
            lst = QD.QuyDinh_MoiNhat();
            lst_LBT = QD.Lst_LoaiBT();
            nudTuoi_Max.Value = lst[0].Tuoi_Max;
            nudTuoi_Min.Value = lst[0].Tuoi_Min;
            nudCauThu_Max.Value = lst[0].SoLuong_Max;
            nudCauThu_Min.Value = lst[0].SoLuong_Min;
            nudNuocNgoai_Max.Value = lst[0].CTN_MAX;
            nudTime_Max.Value = lst[0].Time_Max;
            nudThang.Value = lst[0].Thang;
            nudHoa.Value = lst[0].Hoa;
            nudThua.Value = lst[0].Thua;
            lstBLoaiBanThang.DataSource = lst_LBT;
            lstBLoaiBanThang.DisplayMember = "TenLoai";
            lstBLoaiBanThang.ValueMember = "Id";
        }
        private void GUI_Quy_Định_Load(object sender, EventArgs e)
        {
            load_MoiNhat();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            QD.Insert_LoaiBT(txtTenLoai.Text);
            lst_LBT = QD.Lst_LoaiBT();
            lstBLoaiBanThang.DataSource = lst_LBT;
            lstBLoaiBanThang.DisplayMember = "TenLoai";
            txtTenLoai.Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            QD.Delete_LoaiBT(lstBLoaiBanThang.SelectedValue.ToString());
            lst_LBT = QD.Lst_LoaiBT();
            lstBLoaiBanThang.DataSource = lst_LBT;
            lstBLoaiBanThang.DisplayMember = "TenLoai";          
        }
    }
}
