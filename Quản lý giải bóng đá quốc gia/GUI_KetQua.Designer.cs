﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class GUI_KetQua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvTRANDAU = new System.Windows.Forms.DataGridView();
            this.btnUPDATE = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.txtMaTD = new System.Windows.Forms.TextBox();
            this.txtDoi1 = new System.Windows.Forms.TextBox();
            this.txtDoi2 = new System.Windows.Forms.TextBox();
            this.txtBTDoi1 = new System.Windows.Forms.TextBox();
            this.txtBTDoi2 = new System.Windows.Forms.TextBox();
            this.txtSVD = new System.Windows.Forms.TextBox();
            this.txtNgay = new System.Windows.Forms.TextBox();
            this.txtGio = new System.Windows.Forms.TextBox();
            this.cmbVongTD = new System.Windows.Forms.ComboBox();
            this.txtSTT = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTRANDAU)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã trận đấu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(324, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vòng thi đấu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(171, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Giờ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(294, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sân";
            // 
            // dgvTRANDAU
            // 
            this.dgvTRANDAU.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTRANDAU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTRANDAU.Location = new System.Drawing.Point(3, 123);
            this.dgvTRANDAU.Name = "dgvTRANDAU";
            this.dgvTRANDAU.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvTRANDAU.Size = new System.Drawing.Size(488, 232);
            this.dgvTRANDAU.TabIndex = 5;
            // 
            // btnUPDATE
            // 
            this.btnUPDATE.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnUPDATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUPDATE.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnUPDATE.Location = new System.Drawing.Point(320, 361);
            this.btnUPDATE.Name = "btnUPDATE";
            this.btnUPDATE.Size = new System.Drawing.Size(84, 23);
            this.btnUPDATE.TabIndex = 6;
            this.btnUPDATE.Text = "Cập Nhật";
            this.btnUPDATE.UseVisualStyleBackColor = false;
            this.btnUPDATE.Click += new System.EventHandler(this.btnUPDATE_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnThoat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnThoat.Location = new System.Drawing.Point(416, 361);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 23);
            this.btnThoat.TabIndex = 7;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // txtMaTD
            // 
            this.txtMaTD.Location = new System.Drawing.Point(118, 25);
            this.txtMaTD.Name = "txtMaTD";
            this.txtMaTD.ReadOnly = true;
            this.txtMaTD.Size = new System.Drawing.Size(50, 20);
            this.txtMaTD.TabIndex = 8;
            // 
            // txtDoi1
            // 
            this.txtDoi1.Location = new System.Drawing.Point(39, 51);
            this.txtDoi1.Name = "txtDoi1";
            this.txtDoi1.ReadOnly = true;
            this.txtDoi1.Size = new System.Drawing.Size(168, 20);
            this.txtDoi1.TabIndex = 10;
            // 
            // txtDoi2
            // 
            this.txtDoi2.Location = new System.Drawing.Point(289, 51);
            this.txtDoi2.Name = "txtDoi2";
            this.txtDoi2.ReadOnly = true;
            this.txtDoi2.Size = new System.Drawing.Size(177, 20);
            this.txtDoi2.TabIndex = 11;
            // 
            // txtBTDoi1
            // 
            this.txtBTDoi1.Location = new System.Drawing.Point(213, 51);
            this.txtBTDoi1.Name = "txtBTDoi1";
            this.txtBTDoi1.Size = new System.Drawing.Size(32, 20);
            this.txtBTDoi1.TabIndex = 12;
            // 
            // txtBTDoi2
            // 
            this.txtBTDoi2.Location = new System.Drawing.Point(251, 51);
            this.txtBTDoi2.Name = "txtBTDoi2";
            this.txtBTDoi2.Size = new System.Drawing.Size(32, 20);
            this.txtBTDoi2.TabIndex = 13;
            // 
            // txtSVD
            // 
            this.txtSVD.Location = new System.Drawing.Point(329, 87);
            this.txtSVD.Name = "txtSVD";
            this.txtSVD.Size = new System.Drawing.Size(137, 20);
            this.txtSVD.TabIndex = 14;
            // 
            // txtNgay
            // 
            this.txtNgay.Location = new System.Drawing.Point(78, 87);
            this.txtNgay.Name = "txtNgay";
            this.txtNgay.Size = new System.Drawing.Size(87, 20);
            this.txtNgay.TabIndex = 15;
            // 
            // txtGio
            // 
            this.txtGio.Location = new System.Drawing.Point(203, 87);
            this.txtGio.Name = "txtGio";
            this.txtGio.Size = new System.Drawing.Size(70, 20);
            this.txtGio.TabIndex = 16;
            // 
            // cmbVongTD
            // 
            this.cmbVongTD.FormattingEnabled = true;
            this.cmbVongTD.Location = new System.Drawing.Point(410, 25);
            this.cmbVongTD.Name = "cmbVongTD";
            this.cmbVongTD.Size = new System.Drawing.Size(56, 21);
            this.cmbVongTD.TabIndex = 17;
            this.cmbVongTD.SelectedIndexChanged += new System.EventHandler(this.cmbVongTD_SelectedIndexChanged);
            // 
            // txtSTT
            // 
            this.txtSTT.Location = new System.Drawing.Point(47, 364);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Size = new System.Drawing.Size(33, 20);
            this.txtSTT.TabIndex = 18;
            // 
            // btnNext
            // 
            this.btnNext.BackgroundImage = global::Quản_lý_giải_bóng_đá_quốc_gia.Properties.Resources.Next;
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNext.Location = new System.Drawing.Point(86, 362);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(33, 23);
            this.btnNext.TabIndex = 19;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.BackgroundImage = global::Quản_lý_giải_bóng_đá_quốc_gia.Properties.Resources.Return;
            this.btnReturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReturn.Location = new System.Drawing.Point(8, 362);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(33, 23);
            this.btnReturn.TabIndex = 20;
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // GUI_KetQua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 393);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtSTT);
            this.Controls.Add(this.cmbVongTD);
            this.Controls.Add(this.txtGio);
            this.Controls.Add(this.txtNgay);
            this.Controls.Add(this.txtSVD);
            this.Controls.Add(this.txtBTDoi2);
            this.Controls.Add(this.txtBTDoi1);
            this.Controls.Add(this.txtDoi2);
            this.Controls.Add(this.txtDoi1);
            this.Controls.Add(this.txtMaTD);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnUPDATE);
            this.Controls.Add(this.dgvTRANDAU);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GUI_KetQua";
            this.Text = "Kết Quả Trận Đấu";
            this.Load += new System.EventHandler(this.GUI_KetQua_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTRANDAU)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvTRANDAU;
        private System.Windows.Forms.Button btnUPDATE;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.TextBox txtMaTD;
        private System.Windows.Forms.TextBox txtDoi1;
        private System.Windows.Forms.TextBox txtDoi2;
        private System.Windows.Forms.TextBox txtBTDoi1;
        private System.Windows.Forms.TextBox txtBTDoi2;
        private System.Windows.Forms.TextBox txtSVD;
        private System.Windows.Forms.TextBox txtNgay;
        private System.Windows.Forms.TextBox txtGio;
        private System.Windows.Forms.ComboBox cmbVongTD;
        private System.Windows.Forms.TextBox txtSTT;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnReturn;
    }
}