﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class frmLapBaoCaoGiai : Form
    {
        public frmLapBaoCaoGiai()
        {
            InitializeComponent();
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            if (cbBangXepHang.CheckState == CheckState.Checked)
            {
                GUI_BangXepHang frm = new GUI_BangXepHang();
                frm.Show();
                this.Close();
            }
            else
            {
                GUI_DanhDachCauThu_BanThang frm = new GUI_DanhDachCauThu_BanThang();
                frm.Show();
                this.Close();
            }
        }
    }
}
