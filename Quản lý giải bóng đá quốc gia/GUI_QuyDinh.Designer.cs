﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class GUI_Quy_Định
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.nudNuocNgoai_Max = new System.Windows.Forms.NumericUpDown();
            this.nudCauThu_Max = new System.Windows.Forms.NumericUpDown();
            this.nudTuoi_Max = new System.Windows.Forms.NumericUpDown();
            this.nudCauThu_Min = new System.Windows.Forms.NumericUpDown();
            this.nudTuoi_Min = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUPDATE = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nudThua = new System.Windows.Forms.NumericUpDown();
            this.nudHoa = new System.Windows.Forms.NumericUpDown();
            this.nudThang = new System.Windows.Forms.NumericUpDown();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstBLoaiBanThang = new System.Windows.Forms.ListBox();
            this.nudTime_Max = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTenLoai = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNuocNgoai_Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCauThu_Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTuoi_Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCauThu_Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTuoi_Min)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudThua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTime_Max)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.nudNuocNgoai_Max);
            this.groupBox1.Controls.Add(this.nudCauThu_Max);
            this.groupBox1.Controls.Add(this.nudTuoi_Max);
            this.groupBox1.Controls.Add(this.nudCauThu_Min);
            this.groupBox1.Controls.Add(this.nudTuoi_Min);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 147);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Quy định tiếp nhận hồ sơ đội bóng";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(270, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 16);
            this.label14.TabIndex = 14;
            this.label14.Text = "Đến:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(168, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 16);
            this.label13.TabIndex = 13;
            this.label13.Text = "Từ:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(270, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 12;
            this.label12.Text = "Đến:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(168, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 16);
            this.label11.TabIndex = 11;
            this.label11.Text = "Từ:";
            // 
            // nudNuocNgoai_Max
            // 
            this.nudNuocNgoai_Max.Location = new System.Drawing.Point(315, 106);
            this.nudNuocNgoai_Max.Name = "nudNuocNgoai_Max";
            this.nudNuocNgoai_Max.Size = new System.Drawing.Size(53, 22);
            this.nudNuocNgoai_Max.TabIndex = 9;
            // 
            // nudCauThu_Max
            // 
            this.nudCauThu_Max.Location = new System.Drawing.Point(315, 67);
            this.nudCauThu_Max.Name = "nudCauThu_Max";
            this.nudCauThu_Max.Size = new System.Drawing.Size(53, 22);
            this.nudCauThu_Max.TabIndex = 8;
            // 
            // nudTuoi_Max
            // 
            this.nudTuoi_Max.Location = new System.Drawing.Point(315, 29);
            this.nudTuoi_Max.Name = "nudTuoi_Max";
            this.nudTuoi_Max.Size = new System.Drawing.Size(53, 22);
            this.nudTuoi_Max.TabIndex = 7;
            // 
            // nudCauThu_Min
            // 
            this.nudCauThu_Min.Location = new System.Drawing.Point(200, 67);
            this.nudCauThu_Min.Name = "nudCauThu_Min";
            this.nudCauThu_Min.Size = new System.Drawing.Size(53, 22);
            this.nudCauThu_Min.TabIndex = 6;
            // 
            // nudTuoi_Min
            // 
            this.nudTuoi_Min.Location = new System.Drawing.Point(200, 29);
            this.nudTuoi_Min.Name = "nudTuoi_Min";
            this.nudTuoi_Min.Size = new System.Drawing.Size(53, 22);
            this.nudTuoi_Min.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(411, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 16);
            this.label4.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(207, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số cầu thủ nước ngoài tối đa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số lượng cầu thủ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Độ tuổi cầu thủ:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnUPDATE);
            this.panel1.Controls.Add(this.btnDefault);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 405);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(490, 42);
            this.panel1.TabIndex = 2;
            // 
            // btnUPDATE
            // 
            this.btnUPDATE.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnUPDATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUPDATE.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnUPDATE.Location = new System.Drawing.Point(389, 7);
            this.btnUPDATE.Name = "btnUPDATE";
            this.btnUPDATE.Size = new System.Drawing.Size(79, 32);
            this.btnUPDATE.TabIndex = 1;
            this.btnUPDATE.Text = "Cập nhật";
            this.btnUPDATE.UseVisualStyleBackColor = false;
            this.btnUPDATE.Click += new System.EventHandler(this.btnUPDATE_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefault.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnDefault.Location = new System.Drawing.Point(285, 7);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(79, 32);
            this.btnDefault.TabIndex = 0;
            this.btnDefault.Text = "Mặc định";
            this.btnDefault.UseVisualStyleBackColor = false;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTenLoai);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.nudThua);
            this.groupBox2.Controls.Add(this.nudHoa);
            this.groupBox2.Controls.Add(this.nudThang);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.lstBLoaiBanThang);
            this.groupBox2.Controls.Add(this.nudTime_Max);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(467, 234);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Quy định Bàn thắng_Xếp hạng";
            // 
            // nudThua
            // 
            this.nudThua.Location = new System.Drawing.Point(403, 115);
            this.nudThua.Name = "nudThua";
            this.nudThua.Size = new System.Drawing.Size(53, 22);
            this.nudThua.TabIndex = 14;
            // 
            // nudHoa
            // 
            this.nudHoa.Location = new System.Drawing.Point(403, 78);
            this.nudHoa.Name = "nudHoa";
            this.nudHoa.Size = new System.Drawing.Size(53, 22);
            this.nudHoa.TabIndex = 13;
            // 
            // nudThang
            // 
            this.nudThang.Location = new System.Drawing.Point(403, 38);
            this.nudThang.Name = "nudThang";
            this.nudThang.Size = new System.Drawing.Size(53, 22);
            this.nudThang.TabIndex = 12;
            // 
            // btnDelete
            // 
            this.btnDelete.BackgroundImage = global::Quản_lý_giải_bóng_đá_quốc_gia.Properties.Resources.delete;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.Location = new System.Drawing.Point(253, 155);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(35, 28);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = global::Quản_lý_giải_bóng_đá_quốc_gia.Properties.Resources.add;
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.Location = new System.Drawing.Point(253, 189);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(35, 28);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstBLoaiBanThang
            // 
            this.lstBLoaiBanThang.FormattingEnabled = true;
            this.lstBLoaiBanThang.ItemHeight = 16;
            this.lstBLoaiBanThang.Location = new System.Drawing.Point(34, 99);
            this.lstBLoaiBanThang.Name = "lstBLoaiBanThang";
            this.lstBLoaiBanThang.Size = new System.Drawing.Size(209, 84);
            this.lstBLoaiBanThang.TabIndex = 8;
            // 
            // nudTime_Max
            // 
            this.nudTime_Max.Location = new System.Drawing.Point(216, 38);
            this.nudTime_Max.Name = "nudTime_Max";
            this.nudTime_Max.Size = new System.Drawing.Size(53, 22);
            this.nudTime_Max.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(315, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 5;
            this.label9.Text = "Điểm thua";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(315, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 16);
            this.label8.TabIndex = 4;
            this.label8.Text = "Điểm hòa";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 16);
            this.label7.TabIndex = 3;
            this.label7.Text = "Điểm thắng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(212, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Danh sách các loại bàn thắng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(179, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Thời điểm ghi bàn tối đa:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 16);
            this.label10.TabIndex = 15;
            this.label10.Text = "Loại bàn thắng:";
            // 
            // txtTenLoai
            // 
            this.txtTenLoai.Location = new System.Drawing.Point(120, 192);
            this.txtTenLoai.Name = "txtTenLoai";
            this.txtTenLoai.Size = new System.Drawing.Size(123, 22);
            this.txtTenLoai.TabIndex = 16;
            // 
            // GUI_Quy_Định
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 447);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "GUI_Quy_Định";
            this.Text = "Quy Định";
            this.Load += new System.EventHandler(this.GUI_Quy_Định_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNuocNgoai_Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCauThu_Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTuoi_Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCauThu_Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTuoi_Min)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudThua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTime_Max)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnUPDATE;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudCauThu_Max;
        private System.Windows.Forms.NumericUpDown nudTuoi_Max;
        private System.Windows.Forms.NumericUpDown nudCauThu_Min;
        private System.Windows.Forms.NumericUpDown nudTuoi_Min;
        private System.Windows.Forms.NumericUpDown nudNuocNgoai_Max;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudTime_Max;
        private System.Windows.Forms.ListBox lstBLoaiBanThang;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.NumericUpDown nudThua;
        private System.Windows.Forms.NumericUpDown nudHoa;
        private System.Windows.Forms.NumericUpDown nudThang;
        private System.Windows.Forms.TextBox txtTenLoai;
        private System.Windows.Forms.Label label10;
    }
}