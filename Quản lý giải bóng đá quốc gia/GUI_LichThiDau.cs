﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class GUI_LichThiDau : Form
    {
        BUS_LichThiDau ltd = new BUS_LichThiDau();
        public GUI_LichThiDau()
        {
            InitializeComponent();
            dgvLich.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void btnCreateLTD_Click(object sender, EventArgs e)
        {

            if (ltd.KT_LichThiDau() == true)
            {
                DialogResult dlr = MessageBox.Show("Bạn có chắc muốn tạo mới???",
                                                    "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (ltd.KT_DoiBong() != true)
                    {
                        ltd.XoaLichThiDau();
                        dgvLich.DataSource = ltd.Lich();
                    }
                    else
                        MessageBox.Show("Tồn tại đội bóng không đủ số lượng cầu thủ theo quy định!!!", "Thông Báo",
                                            MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (ltd.KT_DoiBong() != true)
                {
                    dgvLich.DataSource = ltd.Lich();
                }
                else
                    MessageBox.Show("Tồn tại đội bóng không đủ số lượng cầu thủ theo quy định!!!", "Thông Báo",
                                            MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

    }
}
