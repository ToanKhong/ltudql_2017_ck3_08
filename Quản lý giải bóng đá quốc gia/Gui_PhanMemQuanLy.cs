﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class Gui_PhanMemQuanLy : Form
    {
        public Gui_PhanMemQuanLy()
        {
            InitializeComponent();          
        }
        private bool CheckExistForm(string name)
        {
            bool check = false;
            foreach(Form frm in this.MdiChildren)
            {
                if (frm.Name == name)
                {
                    check = true;
                    break;
                }
            }
            return check;
        }
        private void ActiveChildForm(string name)
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (frm.Name == name)
                {
                    frm.Activate();
                    break;
                }
            }
        }
        private void btnHoSoDK_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("GUI_TiepNhanHoSo")))
            {
                GUI_TiepNhanHoSo frm = new GUI_TiepNhanHoSo();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("GUI_TiepNhanHoSo");
            }
        }

        private void btnLichTD_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("GUI_LichThiDau")))
            {
                GUI_LichThiDau frm = new GUI_LichThiDau();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("GUI_LichThiDau");
            }
        }

        private void btnKetQua_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("GUI_KetQua")))
            {
                GUI_KetQua frm = new GUI_KetQua();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("GUI_KetQua");
            }
        }

        private void btnTraCuu_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("GUI_DanhSachCauThuGhiBan")))
            {
                GUI_DanhSachCauThuGhiBan frm = new GUI_DanhSachCauThuGhiBan();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("GUI_DanhSachCauThuGhiBan");
            }
        }

        private void btnLapBaoCao_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("frmLapBaoCaoGiai")))
            {
                frmLapBaoCaoGiai frm = new frmLapBaoCaoGiai();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("frmLapBaoCaoGiai");
            }
        }

        private void btnQuyDinh_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("GUI_Quy_Định")))
            {
                GUI_Quy_Định frm = new GUI_Quy_Định();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("GUI_Quy_Định");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (!(CheckExistForm("frmNew")))
            {
                frmNew frm = new frmNew();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActiveChildForm("frmNew");
            }
        }
    }
}
