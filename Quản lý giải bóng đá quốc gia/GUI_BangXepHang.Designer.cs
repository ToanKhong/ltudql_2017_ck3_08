﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class GUI_BangXepHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvXepHang = new System.Windows.Forms.DataGridView();
            this.cmbNgayTD = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnXuatFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvXepHang)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvXepHang
            // 
            this.dgvXepHang.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvXepHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvXepHang.Location = new System.Drawing.Point(18, 100);
            this.dgvXepHang.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvXepHang.Name = "dgvXepHang";
            this.dgvXepHang.Size = new System.Drawing.Size(936, 492);
            this.dgvXepHang.TabIndex = 0;
            // 
            // cmbNgayTD
            // 
            this.cmbNgayTD.FormattingEnabled = true;
            this.cmbNgayTD.Location = new System.Drawing.Point(430, 37);
            this.cmbNgayTD.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbNgayTD.Name = "cmbNgayTD";
            this.cmbNgayTD.Size = new System.Drawing.Size(180, 28);
            this.cmbNgayTD.TabIndex = 1;
            this.cmbNgayTD.SelectedIndexChanged += new System.EventHandler(this.cmbNgayTD_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(348, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ngày:";
            // 
            // btnXuatFile
            // 
            this.btnXuatFile.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnXuatFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatFile.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnXuatFile.Location = new System.Drawing.Point(720, 23);
            this.btnXuatFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnXuatFile.Name = "btnXuatFile";
            this.btnXuatFile.Size = new System.Drawing.Size(172, 57);
            this.btnXuatFile.TabIndex = 3;
            this.btnXuatFile.Text = "Xuất Báo Cáo";
            this.btnXuatFile.UseVisualStyleBackColor = false;
            this.btnXuatFile.Click += new System.EventHandler(this.btnXuatFile_Click);
            // 
            // GUI_BangXepHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 611);
            this.Controls.Add(this.btnXuatFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbNgayTD);
            this.Controls.Add(this.dgvXepHang);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GUI_BangXepHang";
            this.Text = "Bảng Xếp Hạng";
            ((System.ComponentModel.ISupportInitialize)(this.dgvXepHang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvXepHang;
        private System.Windows.Forms.ComboBox cmbNgayTD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnXuatFile;
    }
}