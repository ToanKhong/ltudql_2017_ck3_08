﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class frmNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbTaoGiaiDauMoi = new System.Windows.Forms.CheckBox();
            this.cbKetQua_BanThang = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbTaoGiaiDauMoi
            // 
            this.cbTaoGiaiDauMoi.AutoSize = true;
            this.cbTaoGiaiDauMoi.Location = new System.Drawing.Point(43, 41);
            this.cbTaoGiaiDauMoi.Name = "cbTaoGiaiDauMoi";
            this.cbTaoGiaiDauMoi.Size = new System.Drawing.Size(149, 20);
            this.cbTaoGiaiDauMoi.TabIndex = 0;
            this.cbTaoGiaiDauMoi.Text = "Tạo Mùa Giải Mới";
            this.cbTaoGiaiDauMoi.UseVisualStyleBackColor = true;
            // 
            // cbKetQua_BanThang
            // 
            this.cbKetQua_BanThang.AutoSize = true;
            this.cbKetQua_BanThang.Location = new System.Drawing.Point(43, 93);
            this.cbKetQua_BanThang.Name = "cbKetQua_BanThang";
            this.cbKetQua_BanThang.Size = new System.Drawing.Size(234, 20);
            this.cbKetQua_BanThang.TabIndex = 1;
            this.cbKetQua_BanThang.Text = "Khởi Tạo Kết Quả_Bàn Thắng ";
            this.cbKetQua_BanThang.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.Location = new System.Drawing.Point(306, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "Xác Nhận";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cbTaoGiaiDauMoi);
            this.groupBox1.Controls.Add(this.cbKetQua_BanThang);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox1.Location = new System.Drawing.Point(32, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 141);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn Thông Tin Muốn Khởi Tạo";
            // 
            // frmNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 198);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmNew";
            this.Text = "frmNew";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox cbTaoGiaiDauMoi;
        private System.Windows.Forms.CheckBox cbKetQua_BanThang;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}