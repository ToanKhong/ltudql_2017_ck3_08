﻿namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    partial class GUI_LichThiDau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvLich = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCreateLTD = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLich)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvLich
            // 
            this.dgvLich.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLich.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLich.Location = new System.Drawing.Point(5, 43);
            this.dgvLich.Name = "dgvLich";
            this.dgvLich.Size = new System.Drawing.Size(627, 308);
            this.dgvLich.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(221, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lịch Thi Đấu";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCreateLTD);
            this.panel2.Location = new System.Drawing.Point(5, 357);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(627, 57);
            this.panel2.TabIndex = 4;
            // 
            // btnCreateLTD
            // 
            this.btnCreateLTD.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCreateLTD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateLTD.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnCreateLTD.Location = new System.Drawing.Point(475, 16);
            this.btnCreateLTD.Name = "btnCreateLTD";
            this.btnCreateLTD.Size = new System.Drawing.Size(144, 32);
            this.btnCreateLTD.TabIndex = 0;
            this.btnCreateLTD.Text = "Tạo Lịch Thi Đấu";
            this.btnCreateLTD.UseVisualStyleBackColor = false;
            this.btnCreateLTD.Click += new System.EventHandler(this.btnCreateLTD_Click);
            // 
            // GUI_LichThiDau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 417);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgvLich);
            this.Name = "GUI_LichThiDau";
            this.Text = "Tạo Lịch Thi Đấu";
            ((System.ComponentModel.ISupportInitialize)(this.dgvLich)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLich;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCreateLTD;
        private System.Windows.Forms.Label label1;
    }
}