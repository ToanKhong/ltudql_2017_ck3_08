﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class GUI_KetQua : Form
    {
        List<TranDau> lst_TranDau = null;
        List<int> lst_VongDau = null;
        BUS_KetQua hs = new BUS_KetQua();
        int count = 0;
        public GUI_KetQua()
        {
            InitializeComponent();
            dgvTRANDAU.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void btnUPDATE_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvTRANDAU.Rows.Count; i++)
            {
                if (dgvTRANDAU.Rows[i].Cells["Cầu Thủ"].Value == null)
                {
                    break;
                }
                else {

                    string TenCT = dgvTRANDAU.Rows[i].Cells["Cầu Thủ"].Value.ToString();
                    string loai = dgvTRANDAU.Rows[i].Cells["Loại Bàn Thắng"].Value.ToString();
                    int ThoiDiem = int.Parse(dgvTRANDAU.Rows[i].Cells["Thời Điểm"].Value.ToString());
                     if (hs.KT_ThoiDiem(ThoiDiem))
                    {
                        MessageBox.Show("Thời điểm quá quy định!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (hs.KT_LoaiBanThang(loai))
                    {
                        MessageBox.Show("Loại bàn thắng không tồn tại!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        hs.UPDATE(txtMaTD.Text, txtBTDoi1.Text, txtBTDoi2.Text, TenCT, loai, ThoiDiem);
                    }
                }
            }
            dgvTRANDAU.DataSource = hs.lst_BanThang(txtMaTD.Text);
            dgvTRANDAU.Columns["STT"].Width = 30;
            dgvTRANDAU.Columns["Loại Bàn Thắng"].Width = 120;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        void load()
        {
            lst_TranDau = hs.load_TranDau(int.Parse(cmbVongTD.Text));
            txtMaTD.Text = lst_TranDau[count].ID;
            txtDoi1.Text = lst_TranDau[count].Doi1;
            txtDoi2.Text = lst_TranDau[count].Doi2;
            txtBTDoi1.Text = lst_TranDau[count].Goal_Doi1;
            txtBTDoi2.Text = lst_TranDau[count].Goal_Doi2;
            txtNgay.Text = lst_TranDau[count].Ngay;
            txtGio.Text = lst_TranDau[count].Gio;
            txtSVD.Text = lst_TranDau[count].SVD;
            txtSTT.Text = (count + 1).ToString() + "/" + lst_TranDau.Count();
            dgvTRANDAU.DataSource = hs.lst_BanThang(txtMaTD.Text);
            dgvTRANDAU.Columns["STT"].Width = 30;
            dgvTRANDAU.Columns["Loại Bàn Thắng"].Width = 120;
            dgvTRANDAU.Columns["STT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void cmbVongTD_SelectedIndexChanged(object sender, EventArgs e)
        {
            count = 0;
            load();
        }

        private void GUI_KetQua_Load(object sender, EventArgs e)
        {
            lst_VongDau = hs.lst_VongDau();
            cmbVongTD.DataSource = lst_VongDau;        
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            int temp = (count + 1);
            if ( temp < lst_TranDau.Count)
            {
                count++;
                txtSTT.Text = (count + 1).ToString() + "/" + lst_TranDau.Count();
                load();                          
            }

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            int temp = (count - 1);
            if (temp >= 0)
            {
                count--;
                txtSTT.Text = (count + 1).ToString() + "/" + lst_TranDau.Count();
                load();
            }
        }
    }
}
