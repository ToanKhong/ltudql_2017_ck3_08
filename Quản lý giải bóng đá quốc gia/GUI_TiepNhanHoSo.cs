﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace Quản_lý_giải_bóng_đá_quốc_gia
{
    public partial class GUI_TiepNhanHoSo : Form
    {
        public delegate void SendInformation(string TenDB,string SVD,string TenCT, string NgaySinh,int loaiCT,string GhiChu);
        public SendInformation Sender;
        BUS_TiepNhanHoSoDangKi hs = new BUS_TiepNhanHoSoDangKi();
        List<FootBall_Club> lstDB = null;
        int soluongDB = 0;
        public GUI_TiepNhanHoSo()
        {
            InitializeComponent();
            dgvHoSo.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            Sender = new SendInformation(GetInformation);
        }
        private void GetInformation(string TenDB, string SVD, string TenCT, string NgaySinh, int loaiCT, string GhiChu)
        {
            hs.Insert_DoiBong(TenDB, SVD);
            hs.Insert_Player(new Player(TenCT, Convert.ToDateTime(NgaySinh), loaiCT, GhiChu), TenDB);
        }
        void load()
        {           
            lstDB = hs.lstDoiBong();
            txtDoiBong.Text = lstDB[soluongDB].TenDB;
            txtSVĐ.Text = lstDB[soluongDB].SanVĐ;
            txtMa.Text = lstDB[soluongDB].ID_DB;
            txtSoLuong.Text = lstDB[soluongDB].SoLuong.ToString();
            txtSTT.Text = (soluongDB + 1)+ "/" + lstDB.Count;
            dgvHoSo.DataSource = hs.Load_Player(txtDoiBong.Text);
            dgvHoSo.Columns["STT"].Width = 30;
            dgvHoSo.Columns["STT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHoSo.Columns["Mã Cầu Thủ"].ReadOnly = true;
        }
        private void GUI_TiepNhanHoSo_Load(object sender, EventArgs e)
        {
            load();
        }
        private void btnReturn_Click(object sender, EventArgs e)
        {
            int temp = soluongDB - 1;
            if (temp >= 0)
            {
                soluongDB--;
                load();
            }       
                
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            int temp = soluongDB + 1;
            if (temp < lstDB.Count)
            {
                soluongDB++;
                load();
            }
        }
        private void btnADD_Click(object sender, EventArgs e)
        {
            frmThem item = new frmThem();
            item.Sender(txtDoiBong.Text,txtSVĐ.Text);
            item.Show();         
        }     

        private void btnDelete_Click(object sender, EventArgs e)
        {          
            int num = dgvHoSo.CurrentCell.RowIndex;           
            hs.Delete_Player(dgvHoSo.Rows[num].Cells["Mã Cầu Thủ"].Value.ToString());
            GUI_TiepNhanHoSo_Load(sender, e);
        }

        private void btnDeleteDB_Click(object sender, EventArgs e)
        {
            hs.Delete_DoiBong(txtMa.Text);
            GUI_TiepNhanHoSo_Load(sender, e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int num = dgvHoSo.CurrentCell.RowIndex;
            string tenCT = dgvHoSo.Rows[num].Cells["Tên Cầu Thủ"].Value.ToString();
            string NgaySinh = dgvHoSo.Rows[num].Cells["Ngày Sinh"].Value.ToString();
            int LoaiCT;
            if (dgvHoSo.Rows[num].Cells["Loại Cầu Thủ"].Value.ToString() == "Cầu thủ ngoại")
                LoaiCT = 0;
            else
                LoaiCT = 1;
            string ghichu = dgvHoSo.Rows[num].Cells["Ghi Chú"].Value.ToString();
            hs.UPDATE(dgvHoSo.Rows[num].Cells["Mã Cầu Thủ"].Value.ToString(),
                new Player(tenCT,Convert.ToDateTime(NgaySinh),LoaiCT,ghichu), 
                new FootBall_Club(txtMa.Text,txtDoiBong.Text,txtSVĐ.Text,int.Parse(txtSoLuong.Text)));
            GUI_TiepNhanHoSo_Load(sender, e);
        }

      
    }
}
